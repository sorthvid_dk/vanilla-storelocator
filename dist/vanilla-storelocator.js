(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 355);
/******/ })
/************************************************************************/
/******/ ({

/***/ 122:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(124);

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),

/***/ 123:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(54)))

/***/ }),

/***/ 124:
/***/ (function(module, exports, __webpack_require__) {

var freeGlobal = __webpack_require__(123);

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),

/***/ 125:
/***/ (function(module, exports) {

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;


/***/ }),

/***/ 126:
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(343),
    isLength = __webpack_require__(127);

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;


/***/ }),

/***/ 127:
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;


/***/ }),

/***/ 128:
/***/ (function(module, exports) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),

/***/ 129:
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ 132:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _index = __webpack_require__(40);

var _settings = __webpack_require__(55);

var _microtemplate = __webpack_require__(48);

var _microtemplate2 = _interopRequireDefault(_microtemplate);

var _events = __webpack_require__(47);

var _events2 = _interopRequireDefault(_events);

var _utils = __webpack_require__(41);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InfoOverlay = function (_MicroTemplate) {
	_inherits(InfoOverlay, _MicroTemplate);

	function InfoOverlay(data) {
		_classCallCheck(this, InfoOverlay);

		var _this = _possibleConstructorReturn(this, (InfoOverlay.__proto__ || Object.getPrototypeOf(InfoOverlay)).call(this, data));

		var events = {};
		events['click ' + _index.defaults.html.overlayClose] = 'onClose';
		_this.delegateEvents(events);

		_events2.default.on(_settings.ev.mapReady, function () {
			_this.el.classList.toggle('is-fixed', _events2.default.state.isSmallScreen);
		});
		_this.isActive = false;
		return _this;
	}

	_createClass(InfoOverlay, [{
		key: 'onClose',
		value: function onClose(e) {
			e.preventDefault();
			this.isActive = false;
			_events2.default.emit(_settings.ev.hideInfo);
			this.el.classList.add(_index.defaults.transitionOut);
		}
	}, {
		key: 'onTransitionEnd',
		value: function onTransitionEnd(e) {
			if (!this.isActive && e.propertyName === 'opacity') {
				this.destroy();
			}
		}

		/**
  *  setActive - set components state to active
  */

	}, {
		key: 'setActive',
		value: function setActive() {
			this.isActive = true;
		}

		/**
  *  getGoogleLink
  *  @private
  *  @param data {Object} - detailModel
  */

	}, {
		key: 'getGoogleLink',
		value: function getGoogleLink(data) {
			var base = '//maps.google.com/?';
			var daddr = 'daddr=' + '+' + data.address2.replace(/ /g, '+') + '+' + data.city.replace(/ /g, '+') + '+' + data.zipCode;
			var saddr = _events2.default.state.geoLocation ? '&saddr=Current Location' : '';
			return base + daddr + saddr;
		}
		/*jshint quotmark: double */

	}, {
		key: 'render',
		value: function render(data) {
			if (Object.keys(data).length === 0 && data.constructor === Object) return null;
			var geoDistance = _events2.default.state.geoLocation ? '<span>' + (data.distanceToGeoLocation + ' ' + _index.defaults.labels.distanceUnit + ' ' + _index.defaults.labels.distance) + ' </span><br />' : '';
			var css = _events2.default.state.isSmallScreen ? ' is-fixed' : '';
			var links = data.links.map(function (link) {
				return '<li>' + link.label + '<a href="' + link.href + '">' + link.text + '</a></li>';
			});
			var openingHours = data.openingHours.map(function (link) {
				return '<dt>' + link.label + '</dt>\n\t\t\t<dd>' + link.hours + '</dd>';
			});
			return '<div class="' + ((0, _utils.formatSelector)(_index.defaults.cssClass.infoOverlay) + css) + '">\n\t\t\t<button class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoOverlayClose) + '" ' + (0, _utils.formatSelector)(_index.defaults.html.overlayClose) + '>\n\t\t\t\t<svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n    \t\t\t<g id="Page-1" fill-rule="evenodd">\n    \t\t\t<polygon id="close" fill="#111111" points="15 14.2848003 14.2924528 14.9989898 7.5 8.21368442 0.70754717 15 0 14.2858105 6.78234501 7.49949492 0 0.714189508 0.70754717 7.1776827e-15 7.5 6.78631558 14.2924528 0 15 0.714189508 8.21765499 7.49949492"></polygon>\n    \t\t\t</g>\n    \t\t\t</svg>\n    \t\t\t<span aria-hidden="true" class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.visibilityHidden) + '">' + _index.defaults.labels.overlayClose + '</span>\n\t\t\t</button>\n\t\t\t<div class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoOverlayContent) + '">\n\t\t\t\t<address class="' + ((0, _utils.formatSelector)(_index.defaults.cssClass.infoSection) + ' ' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoAddress)) + '">\n\t\t\t\t\t<span class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoAddrHeader) + '">' + data.address1 + '</span>\n\t\t\t\t\t' + data.address2 + '<br />\n\t\t\t\t\t' + data.zipCode + ' ' + data.city + '<br />\n\t\t\t\t\t' + geoDistance + '\n\n\t\t\t\t</address>\n\t\t\t\t<div class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoSection) + '">\n\t\t\t\t\t<ul class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoLinklist) + '">\n\t\t\t\t\t\t<li> <a href="' + this.getGoogleLink(data) + '" target="_blank">Get Direction</a></li>\n\t\t\t\t\t\t' + links.join('') + '\n\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t\t<div class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoSection) + '">\n\t\t\t\t\t<dl class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.infoHours) + '">\n\t\t\t\t\t\t' + openingHours.join('') + '\n\t\t\t\t\t</dl>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>';
		}
	}]);

	return InfoOverlay;
}(_microtemplate2.default);

exports.default = InfoOverlay;

/***/ }),

/***/ 133:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#46bcec" }, { "visibility": "on" }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#4f586d" }] }];

/***/ }),

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _index = __webpack_require__(40);

var _microtemplate = __webpack_require__(48);

var _microtemplate2 = _interopRequireDefault(_microtemplate);

var _utils = __webpack_require__(41);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Note = function (_MicroTemplate) {
	_inherits(Note, _MicroTemplate);

	//storeAttributeModel
	function Note(data) {
		_classCallCheck(this, Note);

		var _this = _possibleConstructorReturn(this, (Note.__proto__ || Object.getPrototypeOf(Note)).call(this, data));

		var _listners = {
			'click .button': 'onClick'
		};
		_this.delegateEvents(_listners);
		_this.isSelected = false;
		return _this;
	}

	_createClass(Note, [{
		key: 'onClick',
		value: function onClick(e) {
			this.data.onClick(e);
		}
	}, {
		key: 'render',
		value: function render(data) {
			return '<li class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.note) + '">\n\t\t\t<h2>' + data.title + '</h2>\n\t\t\t<span class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.noteSubTitle) + '">' + data.subTitle + '</span>\n\t\t\t<a href="#" class="button button-primary">' + data.button.text + '</a>\n\t\t</li>';
		}
	}]);

	return Note;
}(_microtemplate2.default);

exports.default = Note;

/***/ }),

/***/ 135:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _index = __webpack_require__(40);

var _utils = __webpack_require__(41);

var _microtemplate = __webpack_require__(48);

var _microtemplate2 = _interopRequireDefault(_microtemplate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Progress = function (_MicroTemplate) {
	_inherits(Progress, _MicroTemplate);

	function Progress(data) {
		_classCallCheck(this, Progress);

		var _this = _possibleConstructorReturn(this, (Progress.__proto__ || Object.getPrototypeOf(Progress)).call(this, data));

		_this.body = document.body;
		_this.delegateEvents({
			'transitionend': 'onTransitionEndRemove',
			'webkitTransitionEnd': 'onTransitionEndRemove',
			'click': 'onTransitionEndRemove'
		});
		return _this;
	}

	_createClass(Progress, [{
		key: 'onTransitionEndRemove',
		value: function onTransitionEndRemove() {
			this.el.classList.remove('fade-out');
			_get(Progress.prototype.__proto__ || Object.getPrototypeOf(Progress.prototype), 'destroy', this).call(this);
		}
	}, {
		key: 'start',
		value: function start(data) {
			if (data) {
				this.reRender(data);
			}
			this.body.appendChild(this.el);
		}
	}, {
		key: 'stop',
		value: function stop() {
			this.el.classList.add('fade-out');
		}
		/*jshint quotmark: double */

	}, {
		key: 'render',
		value: function render(data) {
			var message = data.message || '';
			return '<div class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.progress) + '">\n\t\t\t<div class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.progressMsg) + '">\n\t\t\t\t<img src="' + _index.defaults.spinnerUrl + '" alt="image of a waiting graphic" />\n\t\t\t\t<p>' + message + '</p>\n\n\t\t\t</div>\n\t\t</div>';
		}
	}]);

	return Progress;
}(_microtemplate2.default);

exports.default = Progress;

/***/ }),

/***/ 136:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _index = __webpack_require__(40);

var _settings = __webpack_require__(55);

var _microtemplate = __webpack_require__(48);

var _microtemplate2 = _interopRequireDefault(_microtemplate);

var _events = __webpack_require__(47);

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var StoreAttribute = function (_MicroTemplate) {
	_inherits(StoreAttribute, _MicroTemplate);

	//storeAttributeModel
	function StoreAttribute(data) {
		_classCallCheck(this, StoreAttribute);

		var _this = _possibleConstructorReturn(this, (StoreAttribute.__proto__ || Object.getPrototypeOf(StoreAttribute)).call(this, data));

		var _listners = {
			click: 'onClick'
		};
		_this.delegateEvents(_listners);
		_this.isSelected = false;
		return _this;
	}

	_createClass(StoreAttribute, [{
		key: 'onClick',
		value: function onClick(e) {
			var _this2 = this;

			e.preventDefault();
			this.isSelected = !this.isSelected;
			var activeFilters = [].concat(_events2.default.state.activeFilters);

			if (this.isSelected) {
				activeFilters.push(this.data.sid);
			} else {
				//activeFilters = Immutable.without(activeFilters, this.data.id);
				activeFilters = activeFilters.filter(function (item) {
					return item != _this2.data.sid;
				});
			}
			_events2.default.setState(_settings.ev.setFilter, ['activeFilters'], activeFilters, true);
			this.el.classList.toggle(_index.defaults.cssClass.isSelected, this.isSelected);
		}
	}, {
		key: 'render',
		value: function render(data) {
			return '<a href="#" role="button">\n\t\t\t<img src="' + data.filterIcon + '" /><span>' + data.name + '</span>\n\t\t</a>';
		}
	}]);

	return StoreAttribute;
}(_microtemplate2.default);

exports.default = StoreAttribute;

/***/ }),

/***/ 137:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _index = __webpack_require__(40);

var _settings = __webpack_require__(55);

var _utils = __webpack_require__(41);

var _microtemplate = __webpack_require__(48);

var _microtemplate2 = _interopRequireDefault(_microtemplate);

var _events = __webpack_require__(47);

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Store = function (_MicroTemplate) {
	_inherits(Store, _MicroTemplate);

	function Store(data, map) {
		_classCallCheck(this, Store);

		var _this = _possibleConstructorReturn(this, (Store.__proto__ || Object.getPrototypeOf(Store)).call(this, data));

		_this.isSelected = _events2.default.state.selectedStore === _this.data.id;
		_this.isVisible = true;
		_this.map = map;

		_this.storeType = _index.defaults.storeAttributes.filter(function (storeAttr) {
			return storeAttr.sid == _this.data.sid;
		});
		_this.markerIcons = {
			default: new google.maps.MarkerImage(_this.storeType[0].markerIcon.url, null, null, null, new google.maps.Size(_this.storeType[0].markerIcon.w, _this.storeType[0].markerIcon.h)),
			selected: new google.maps.MarkerImage(_this.storeType[0].markerIconSelected.url, null, null, null, new google.maps.Size(_this.storeType[0].markerIconSelected.w, _this.storeType[0].markerIconSelected.h)),
			hover: _this.storeType[0].markerIconHover ? new google.maps.MarkerImage(_this.storeType[0].markerIconHover.url, null, null, null, new google.maps.Size(_this.storeType[0].markerIconHover.w, _this.storeType[0].markerIconHover.h)) : null
		};
		_this.eventCache = [];
		_this.setMarker(_this.map);

		// html events
		var _listners = {
			'click': 'onListItemClick',
			'mouseover': 'onMouseOver',
			'mouseout': 'onMouseOut'
		};
		_listners['click ' + _index.defaults.html.infoItem] = 'onInfoClick';
		_this.delegateEvents(_listners);

		//App events

		_this._onMarkerClick = _this.onMarkerClick.bind(_this);
		_this._setSelection = _this.setSelection.bind(_this);
		_this._destroy = _this.destroy.bind(_this);

		_events2.default.on(_settings.ev.markerClick, _this._onMarkerClick);
		_events2.default.on(_settings.ev.selectedStore, _this._setSelection);
		_events2.default.on(_settings.ev.clearList, _this._destroy);

		// map is draged
		_this.eventCache.push(_this.map.addListener('dragend', _this.onMapChange.bind(_this)));
		// zoom is changed
		_this.eventCache.push(_this.map.addListener('zoom_changed', _this.onMapChange.bind(_this)));
		_this.updateView({ setCenter: false });

		return _this;
	}

	/**
 * setMarkers is called on instantiation
 *
 */


	_createClass(Store, [{
		key: 'setMarker',
		value: function setMarker(map) {
			var _this2 = this;

			var pos = new google.maps.LatLng(this.data.latitude, this.data.longitude);
			this.marker = new google.maps.Marker({
				position: pos,
				map: map,
				icon: this.markerIcons.default,
				clickable: true,
				id: this.data.id,
				optimized: false
			});
			this.eventCache.push(google.maps.event.addListener(this.marker, 'click', function () {
				_events2.default.emit(_settings.ev.markerClick, _this2);
			}));
			this.eventCache.push(google.maps.event.addListener(this.marker, 'mouseover', this.onMouseOver.bind(this)));
			this.eventCache.push(google.maps.event.addListener(this.marker, 'mouseout', this.onMouseOut.bind(this)));
		}

		/**
  * onMarkerClick
  *
  */

	}, {
		key: 'onMarkerClick',
		value: function onMarkerClick(e) {
			this.isSelected = this.data.id === e.data.id;
			this.updateView({ setCenter: true });
			if (this.isSelected) {
				this.onInfoClick(e);
				_events2.default.setState(_settings.ev.selectedStore, ['selectedStore'], this.data.id);
			}
		}
	}, {
		key: 'onListItemClick',
		value: function onListItemClick(e) {
			e.preventDefault();
			_events2.default.setState(_settings.ev.selectedStore, ['selectedStore'], this.data.id);
		}

		/** TODO NOT USED AT THE MOMENT
  * calculate the shop visibility by the default setting 'storeRadius'. if a Place search is performed it will overrule the storeRadius setting.
  *
  */

	}, {
		key: 'onMapChange',
		value: function onMapChange() {}
		//var onMap = this.map.getBounds().contains(this.marker.getPosition());
		//console.log('STORE - onmapchange');
		//this.toggleState(onMap)


		/**
  * afterRender - runs after component template is recompiled
  * @private
  */

	}, {
		key: 'afterReRender',
		value: function afterReRender() {
			this.isSelected = _events2.default.state.selectedStore === this.data.id;
			this.updateView({ setCenter: false });
		}

		/**
  * onInfoClick
  * @private
  */

	}, {
		key: 'onInfoClick',
		value: function onInfoClick(e) {
			if (e.stopPropagation) e.stopPropagation();
			_events2.default.setState(_settings.ev.showInfo, ['openInfoOverlay'], this.data.id);
		}

		/**
  *  on_SetSelection
  */

	}, {
		key: 'setSelection',
		value: function setSelection(state) {
			if (!this.marker) {
				console.warn('event error looks like you have a some garbage listners');
				return;
			}
			this.isSelected = this.data.id === state.selectedStore;
			this.updateView({ setCenter: true });
		}

		/*
  * updates view
  * @private
  */

	}, {
		key: 'updateView',
		value: function updateView(_ref) {
			var _ref$setCenter = _ref.setCenter,
			    setCenter = _ref$setCenter === undefined ? true : _ref$setCenter;

			//highlight list item
			this.el.classList.toggle(_index.defaults.cssClass.isActive, this.isSelected);
			//change marker
			if (this.isSelected) {
				this.marker.setIcon(this.markerIcons.selected);
				if (setCenter) {
					this.map.setCenter(new google.maps.LatLng(this.marker.position.lat(), this.marker.position.lng()));
				}
			} else {
				this.marker.setIcon(this.markerIcons.default);
			}
		}

		/**
  * onMouseOver
  * @private
  */

	}, {
		key: 'onMouseOver',
		value: function onMouseOver() {
			this.el.classList.add(_index.defaults.cssClass.isHover);
			if (!this.isSelected) {
				var icon = this.markerIcons.hover ? this.markerIcons.hover : this.markerIcons.selected;
				this.marker.setIcon(icon);
			}
		}

		/**
  * onMouseOut
  * @private
  */

	}, {
		key: 'onMouseOut',
		value: function onMouseOut() {
			this.el.classList.remove(_index.defaults.cssClass.isHover);
			if (!this.isSelected) {
				this.marker.setIcon(this.markerIcons.default);
			}
		}

		/**
  * setActive - public function to set stores as selected
  * @public
  */

	}, {
		key: 'setSelected',
		value: function setSelected() {
			_events2.default.setState(_settings.ev.selectedStore, ['selectedStore'], this.data.id);
			//Events.setState()
		}
		/**
  * destroy
  * @public
  */

	}, {
		key: 'destroy',
		value: function destroy() {
			this.eventCache.forEach(function (item) {
				google.maps.event.removeListener(item);
			});

			_events2.default.off(_settings.ev.markerClick, this._onMarkerClick);
			_events2.default.off(_settings.ev.selectedStore, this._setSelection);
			_events2.default.off(_settings.ev.clearList, this._destroy);

			this.marker.setMap(null);
			this.marker = null;
			_get(Store.prototype.__proto__ || Object.getPrototypeOf(Store.prototype), 'destroy', this).call(this);
		}

		/**
  * render
  * @private
  */
		/*jshint quotmark: double */

	}, {
		key: 'render',
		value: function render(data) {
			var _this3 = this;

			var distance = function distance() {
				if (data.distanceToGeoLocation) {
					return '<span>' + (data.distanceToGeoLocation + ' ' + _index.defaults.labels.distanceUnit + ' ' + _index.defaults.labels.distance) + ' </span>';
				} else {
					return '';
				}
			};
			var icon = function icon() {
				var storeIcons = _index.defaults.storeAttributes.filter(function (storeAttr) {
					return storeAttr.sid === _this3.data.sid;
				});
				if (storeIcons.length) {
					return '<img src="' + storeIcons[0].filterIcon + '" alt="Store icon">';
				} else {
					return '';
				}
			};
			return '<li role="button" ' + (0, _utils.formatSelector)(_index.defaults.html.listItem) + '>\n\t\t\t<article class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.storeItem) + '">\n\t\t\t\t' + icon() + '\n\t\t\t\t\t<div class="' + (0, _utils.formatSelector)(_index.defaults.cssClass.storeItemInner) + '">\n\t\t\t\t\t<h3 class="title">' + data.address1 + '</h3>\n\t\t\t\t\t<p>' + data.address2 + '</p>\n\t\t\t\t\t<p>' + data.zipCode + ' ' + data.city + '</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t' + distance() + '\n\t\t\t\t\t\t<a href="#" ' + (0, _utils.formatSelector)(_index.defaults.html.infoItem) + '>' + _index.defaults.labels.moreInfo + '</a>\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t</article>\n\t\t</li>';
		}
	}]);

	return Store;
}(_microtemplate2.default);

exports.default = Store;

/***/ }),

/***/ 319:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DEFAULT_VALUES = {
    emitDelay: 10,
    strictMode: false
};

/**
 * @typedef {object} EventEmitterListenerFunc
 * @property {boolean} once
 * @property {function} fn
 */

/**
 * @class EventEmitter
 *
 * @private
 * @property {Object.<string, EventEmitterListenerFunc[]>} _listeners
 * @property {string[]} events
 */

var EventEmitter = function () {

    /**
     * @constructor
     * @param {{}}      [opts]
     * @param {number}  [opts.emitDelay = 10] - Number in ms. Specifies whether emit will be sync or async. By default - 10ms. If 0 - fires sync
     * @param {boolean} [opts.strictMode = false] - is true, Emitter throws error on emit error with no listeners
     */

    function EventEmitter() {
        var opts = arguments.length <= 0 || arguments[0] === undefined ? DEFAULT_VALUES : arguments[0];

        _classCallCheck(this, EventEmitter);

        var emitDelay = void 0,
            strictMode = void 0;

        if (opts.hasOwnProperty('emitDelay')) {
            emitDelay = opts.emitDelay;
        } else {
            emitDelay = DEFAULT_VALUES.emitDelay;
        }
        this._emitDelay = emitDelay;

        if (opts.hasOwnProperty('strictMode')) {
            strictMode = opts.strictMode;
        } else {
            strictMode = DEFAULT_VALUES.strictMode;
        }
        this._strictMode = strictMode;

        this._listeners = {};
        this.events = [];
    }

    /**
     * @protected
     * @param {string} type
     * @param {function} listener
     * @param {boolean} [once = false]
     */


    _createClass(EventEmitter, [{
        key: '_addListenner',
        value: function _addListenner(type, listener, once) {
            if (typeof listener !== 'function') {
                throw TypeError('listener must be a function');
            }

            if (this.events.indexOf(type) === -1) {
                this._listeners[type] = [{
                    once: once,
                    fn: listener
                }];
                this.events.push(type);
            } else {
                this._listeners[type].push({
                    once: once,
                    fn: listener
                });
            }
        }

        /**
         * Subscribes on event type specified function
         * @param {string} type
         * @param {function} listener
         */

    }, {
        key: 'on',
        value: function on(type, listener) {
            this._addListenner(type, listener, false);
        }

        /**
         * Subscribes on event type specified function to fire only once
         * @param {string} type
         * @param {function} listener
         */

    }, {
        key: 'once',
        value: function once(type, listener) {
            this._addListenner(type, listener, true);
        }

        /**
         * Removes event with specified type. If specified listenerFunc - deletes only one listener of specified type
         * @param {string} eventType
         * @param {function} [listenerFunc]
         */

    }, {
        key: 'off',
        value: function off(eventType, listenerFunc) {
            var _this = this;

            var typeIndex = this.events.indexOf(eventType);
            var hasType = eventType && typeIndex !== -1;

            if (hasType) {
                if (!listenerFunc) {
                    delete this._listeners[eventType];
                    this.events.splice(typeIndex, 1);
                } else {
                    (function () {
                        var removedEvents = [];
                        var typeListeners = _this._listeners[eventType];

                        typeListeners.forEach(
                        /**
                         * @param {EventEmitterListenerFunc} fn
                         * @param {number} idx
                         */
                        function (fn, idx) {
                            if (fn.fn === listenerFunc) {
                                removedEvents.unshift(idx);
                            }
                        });

                        removedEvents.forEach(function (idx) {
                            typeListeners.splice(idx, 1);
                        });

                        if (!typeListeners.length) {
                            _this.events.splice(typeIndex, 1);
                            delete _this._listeners[eventType];
                        }
                    })();
                }
            }
        }

        /**
         * Applies arguments to specified event type
         * @param {string} eventType
         * @param {*[]} eventArguments
         * @protected
         */

    }, {
        key: '_applyEvents',
        value: function _applyEvents(eventType, eventArguments) {
            var typeListeners = this._listeners[eventType];

            if (!typeListeners || !typeListeners.length) {
                if (this._strictMode) {
                    throw 'No listeners specified for event: ' + eventType;
                } else {
                    return;
                }
            }

            var removableListeners = [];
            typeListeners.forEach(function (eeListener, idx) {
                eeListener.fn.apply(null, eventArguments);
                if (eeListener.once) {
                    removableListeners.unshift(idx);
                }
            });

            removableListeners.forEach(function (idx) {
                typeListeners.splice(idx, 1);
            });
        }

        /**
         * Emits event with specified type and params.
         * @param {string} type
         * @param eventArgs
         */

    }, {
        key: 'emit',
        value: function emit(type) {
            var _this2 = this;

            for (var _len = arguments.length, eventArgs = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                eventArgs[_key - 1] = arguments[_key];
            }

            if (this._emitDelay) {
                setTimeout(function () {
                    _this2._applyEvents.call(_this2, type, eventArgs);
                }, this._emitDelay);
            } else {
                this._applyEvents(type, eventArgs);
            }
        }

        /**
         * Emits event with specified type and params synchronously.
         * @param {string} type
         * @param eventArgs
         */

    }, {
        key: 'emitSync',
        value: function emitSync(type) {
            for (var _len2 = arguments.length, eventArgs = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
                eventArgs[_key2 - 1] = arguments[_key2];
            }

            this._applyEvents(type, eventArgs);
        }

        /**
         * Destroys EventEmitter
         */

    }, {
        key: 'destroy',
        value: function destroy() {
            this._listeners = {};
            this.events = [];
        }
    }]);

    return EventEmitter;
}();

module.exports = EventEmitter;


/***/ }),

/***/ 320:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/*** IMPORTS FROM imports-loader ***/
(function() {

(function(self) {
  'use strict';

  if (self.fetch) {
    return
  }

  var support = {
    searchParams: 'URLSearchParams' in self,
    iterable: 'Symbol' in self && 'iterator' in Symbol,
    blob: 'FileReader' in self && 'Blob' in self && (function() {
      try {
        new Blob()
        return true
      } catch(e) {
        return false
      }
    })(),
    formData: 'FormData' in self,
    arrayBuffer: 'ArrayBuffer' in self
  }

  if (support.arrayBuffer) {
    var viewClasses = [
      '[object Int8Array]',
      '[object Uint8Array]',
      '[object Uint8ClampedArray]',
      '[object Int16Array]',
      '[object Uint16Array]',
      '[object Int32Array]',
      '[object Uint32Array]',
      '[object Float32Array]',
      '[object Float64Array]'
    ]

    var isDataView = function(obj) {
      return obj && DataView.prototype.isPrototypeOf(obj)
    }

    var isArrayBufferView = ArrayBuffer.isView || function(obj) {
      return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1
    }
  }

  function normalizeName(name) {
    if (typeof name !== 'string') {
      name = String(name)
    }
    if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
      throw new TypeError('Invalid character in header field name')
    }
    return name.toLowerCase()
  }

  function normalizeValue(value) {
    if (typeof value !== 'string') {
      value = String(value)
    }
    return value
  }

  // Build a destructive iterator for the value list
  function iteratorFor(items) {
    var iterator = {
      next: function() {
        var value = items.shift()
        return {done: value === undefined, value: value}
      }
    }

    if (support.iterable) {
      iterator[Symbol.iterator] = function() {
        return iterator
      }
    }

    return iterator
  }

  function Headers(headers) {
    this.map = {}

    if (headers instanceof Headers) {
      headers.forEach(function(value, name) {
        this.append(name, value)
      }, this)

    } else if (headers) {
      Object.getOwnPropertyNames(headers).forEach(function(name) {
        this.append(name, headers[name])
      }, this)
    }
  }

  Headers.prototype.append = function(name, value) {
    name = normalizeName(name)
    value = normalizeValue(value)
    var oldValue = this.map[name]
    this.map[name] = oldValue ? oldValue+','+value : value
  }

  Headers.prototype['delete'] = function(name) {
    delete this.map[normalizeName(name)]
  }

  Headers.prototype.get = function(name) {
    name = normalizeName(name)
    return this.has(name) ? this.map[name] : null
  }

  Headers.prototype.has = function(name) {
    return this.map.hasOwnProperty(normalizeName(name))
  }

  Headers.prototype.set = function(name, value) {
    this.map[normalizeName(name)] = normalizeValue(value)
  }

  Headers.prototype.forEach = function(callback, thisArg) {
    for (var name in this.map) {
      if (this.map.hasOwnProperty(name)) {
        callback.call(thisArg, this.map[name], name, this)
      }
    }
  }

  Headers.prototype.keys = function() {
    var items = []
    this.forEach(function(value, name) { items.push(name) })
    return iteratorFor(items)
  }

  Headers.prototype.values = function() {
    var items = []
    this.forEach(function(value) { items.push(value) })
    return iteratorFor(items)
  }

  Headers.prototype.entries = function() {
    var items = []
    this.forEach(function(value, name) { items.push([name, value]) })
    return iteratorFor(items)
  }

  if (support.iterable) {
    Headers.prototype[Symbol.iterator] = Headers.prototype.entries
  }

  function consumed(body) {
    if (body.bodyUsed) {
      return Promise.reject(new TypeError('Already read'))
    }
    body.bodyUsed = true
  }

  function fileReaderReady(reader) {
    return new Promise(function(resolve, reject) {
      reader.onload = function() {
        resolve(reader.result)
      }
      reader.onerror = function() {
        reject(reader.error)
      }
    })
  }

  function readBlobAsArrayBuffer(blob) {
    var reader = new FileReader()
    var promise = fileReaderReady(reader)
    reader.readAsArrayBuffer(blob)
    return promise
  }

  function readBlobAsText(blob) {
    var reader = new FileReader()
    var promise = fileReaderReady(reader)
    reader.readAsText(blob)
    return promise
  }

  function readArrayBufferAsText(buf) {
    var view = new Uint8Array(buf)
    var chars = new Array(view.length)

    for (var i = 0; i < view.length; i++) {
      chars[i] = String.fromCharCode(view[i])
    }
    return chars.join('')
  }

  function bufferClone(buf) {
    if (buf.slice) {
      return buf.slice(0)
    } else {
      var view = new Uint8Array(buf.byteLength)
      view.set(new Uint8Array(buf))
      return view.buffer
    }
  }

  function Body() {
    this.bodyUsed = false

    this._initBody = function(body) {
      this._bodyInit = body
      if (!body) {
        this._bodyText = ''
      } else if (typeof body === 'string') {
        this._bodyText = body
      } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
        this._bodyBlob = body
      } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
        this._bodyFormData = body
      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
        this._bodyText = body.toString()
      } else if (support.arrayBuffer && support.blob && isDataView(body)) {
        this._bodyArrayBuffer = bufferClone(body.buffer)
        // IE 10-11 can't handle a DataView body.
        this._bodyInit = new Blob([this._bodyArrayBuffer])
      } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
        this._bodyArrayBuffer = bufferClone(body)
      } else {
        throw new Error('unsupported BodyInit type')
      }

      if (!this.headers.get('content-type')) {
        if (typeof body === 'string') {
          this.headers.set('content-type', 'text/plain;charset=UTF-8')
        } else if (this._bodyBlob && this._bodyBlob.type) {
          this.headers.set('content-type', this._bodyBlob.type)
        } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
          this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8')
        }
      }
    }

    if (support.blob) {
      this.blob = function() {
        var rejected = consumed(this)
        if (rejected) {
          return rejected
        }

        if (this._bodyBlob) {
          return Promise.resolve(this._bodyBlob)
        } else if (this._bodyArrayBuffer) {
          return Promise.resolve(new Blob([this._bodyArrayBuffer]))
        } else if (this._bodyFormData) {
          throw new Error('could not read FormData body as blob')
        } else {
          return Promise.resolve(new Blob([this._bodyText]))
        }
      }

      this.arrayBuffer = function() {
        if (this._bodyArrayBuffer) {
          return consumed(this) || Promise.resolve(this._bodyArrayBuffer)
        } else {
          return this.blob().then(readBlobAsArrayBuffer)
        }
      }
    }

    this.text = function() {
      var rejected = consumed(this)
      if (rejected) {
        return rejected
      }

      if (this._bodyBlob) {
        return readBlobAsText(this._bodyBlob)
      } else if (this._bodyArrayBuffer) {
        return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer))
      } else if (this._bodyFormData) {
        throw new Error('could not read FormData body as text')
      } else {
        return Promise.resolve(this._bodyText)
      }
    }

    if (support.formData) {
      this.formData = function() {
        return this.text().then(decode)
      }
    }

    this.json = function() {
      return this.text().then(JSON.parse)
    }

    return this
  }

  // HTTP methods whose capitalization should be normalized
  var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT']

  function normalizeMethod(method) {
    var upcased = method.toUpperCase()
    return (methods.indexOf(upcased) > -1) ? upcased : method
  }

  function Request(input, options) {
    options = options || {}
    var body = options.body

    if (input instanceof Request) {
      if (input.bodyUsed) {
        throw new TypeError('Already read')
      }
      this.url = input.url
      this.credentials = input.credentials
      if (!options.headers) {
        this.headers = new Headers(input.headers)
      }
      this.method = input.method
      this.mode = input.mode
      if (!body && input._bodyInit != null) {
        body = input._bodyInit
        input.bodyUsed = true
      }
    } else {
      this.url = String(input)
    }

    this.credentials = options.credentials || this.credentials || 'omit'
    if (options.headers || !this.headers) {
      this.headers = new Headers(options.headers)
    }
    this.method = normalizeMethod(options.method || this.method || 'GET')
    this.mode = options.mode || this.mode || null
    this.referrer = null

    if ((this.method === 'GET' || this.method === 'HEAD') && body) {
      throw new TypeError('Body not allowed for GET or HEAD requests')
    }
    this._initBody(body)
  }

  Request.prototype.clone = function() {
    return new Request(this, { body: this._bodyInit })
  }

  function decode(body) {
    var form = new FormData()
    body.trim().split('&').forEach(function(bytes) {
      if (bytes) {
        var split = bytes.split('=')
        var name = split.shift().replace(/\+/g, ' ')
        var value = split.join('=').replace(/\+/g, ' ')
        form.append(decodeURIComponent(name), decodeURIComponent(value))
      }
    })
    return form
  }

  function parseHeaders(rawHeaders) {
    var headers = new Headers()
    rawHeaders.split(/\r?\n/).forEach(function(line) {
      var parts = line.split(':')
      var key = parts.shift().trim()
      if (key) {
        var value = parts.join(':').trim()
        headers.append(key, value)
      }
    })
    return headers
  }

  Body.call(Request.prototype)

  function Response(bodyInit, options) {
    if (!options) {
      options = {}
    }

    this.type = 'default'
    this.status = 'status' in options ? options.status : 200
    this.ok = this.status >= 200 && this.status < 300
    this.statusText = 'statusText' in options ? options.statusText : 'OK'
    this.headers = new Headers(options.headers)
    this.url = options.url || ''
    this._initBody(bodyInit)
  }

  Body.call(Response.prototype)

  Response.prototype.clone = function() {
    return new Response(this._bodyInit, {
      status: this.status,
      statusText: this.statusText,
      headers: new Headers(this.headers),
      url: this.url
    })
  }

  Response.error = function() {
    var response = new Response(null, {status: 0, statusText: ''})
    response.type = 'error'
    return response
  }

  var redirectStatuses = [301, 302, 303, 307, 308]

  Response.redirect = function(url, status) {
    if (redirectStatuses.indexOf(status) === -1) {
      throw new RangeError('Invalid status code')
    }

    return new Response(null, {status: status, headers: {location: url}})
  }

  self.Headers = Headers
  self.Request = Request
  self.Response = Response

  self.fetch = function(input, init) {
    return new Promise(function(resolve, reject) {
      var request = new Request(input, init)
      var xhr = new XMLHttpRequest()

      xhr.onload = function() {
        var options = {
          status: xhr.status,
          statusText: xhr.statusText,
          headers: parseHeaders(xhr.getAllResponseHeaders() || '')
        }
        options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL')
        var body = 'response' in xhr ? xhr.response : xhr.responseText
        resolve(new Response(body, options))
      }

      xhr.onerror = function() {
        reject(new TypeError('Network request failed'))
      }

      xhr.ontimeout = function() {
        reject(new TypeError('Network request failed'))
      }

      xhr.open(request.method, request.url, true)

      if (request.credentials === 'include') {
        xhr.withCredentials = true
      }

      if ('responseType' in xhr && support.blob) {
        xhr.responseType = 'blob'
      }

      request.headers.forEach(function(value, name) {
        xhr.setRequestHeader(name, value)
      })

      xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit)
    })
  }
  self.fetch.polyfill = true
})(typeof self !== 'undefined' ? self : this);


/*** EXPORTS FROM exports-loader ***/
module.exports = global.fetch;
}.call(global));
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(54)))

/***/ }),

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

var baseTimes = __webpack_require__(329),
    isArguments = __webpack_require__(341),
    isArray = __webpack_require__(125),
    isBuffer = __webpack_require__(342),
    isIndex = __webpack_require__(333),
    isTypedArray = __webpack_require__(346);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = arrayLikeKeys;


/***/ }),

/***/ 322:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;


/***/ }),

/***/ 323:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.findIndex` and `_.findLastIndex` without
 * support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} predicate The function invoked per iteration.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseFindIndex(array, predicate, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 1 : -1);

  while ((fromRight ? index-- : ++index < length)) {
    if (predicate(array[index], index, array)) {
      return index;
    }
  }
  return -1;
}

module.exports = baseFindIndex;


/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

var baseFindIndex = __webpack_require__(323),
    baseIsNaN = __webpack_require__(326),
    strictIndexOf = __webpack_require__(339);

/**
 * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseIndexOf(array, value, fromIndex) {
  return value === value
    ? strictIndexOf(array, value, fromIndex)
    : baseFindIndex(array, baseIsNaN, fromIndex);
}

module.exports = baseIndexOf;


/***/ }),

/***/ 325:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(52),
    isObjectLike = __webpack_require__(53);

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

module.exports = baseIsArguments;


/***/ }),

/***/ 326:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.isNaN` without support for number objects.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
 */
function baseIsNaN(value) {
  return value !== value;
}

module.exports = baseIsNaN;


/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(52),
    isLength = __webpack_require__(127),
    isObjectLike = __webpack_require__(53);

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

module.exports = baseIsTypedArray;


/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

var isPrototype = __webpack_require__(334),
    nativeKeys = __webpack_require__(335);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeys;


/***/ }),

/***/ 329:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

module.exports = baseTimes;


/***/ }),

/***/ 330:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

module.exports = baseUnary;


/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

var arrayMap = __webpack_require__(322);

/**
 * The base implementation of `_.values` and `_.valuesIn` which creates an
 * array of `object` property values corresponding to the property names
 * of `props`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array} props The property names to get values for.
 * @returns {Object} Returns the array of property values.
 */
function baseValues(object, props) {
  return arrayMap(props, function(key) {
    return object[key];
  });
}

module.exports = baseValues;


/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(122);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),

/***/ 333:
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  length = length == null ? MAX_SAFE_INTEGER : length;
  return !!length &&
    (typeof value == 'number' || reIsUint.test(value)) &&
    (value > -1 && value % 1 == 0 && value < length);
}

module.exports = isIndex;


/***/ }),

/***/ 334:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

module.exports = isPrototype;


/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(338);

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

module.exports = nativeKeys;


/***/ }),

/***/ 336:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var freeGlobal = __webpack_require__(123);

/** Detect free variable `exports`. */
var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

module.exports = nodeUtil;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(129)(module)))

/***/ }),

/***/ 337:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),

/***/ 338:
/***/ (function(module, exports) {

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;


/***/ }),

/***/ 339:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.indexOf` which performs strict equality
 * comparisons of values, i.e. `===`.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function strictIndexOf(array, value, fromIndex) {
  var index = fromIndex - 1,
      length = array.length;

  while (++index < length) {
    if (array[index] === value) {
      return index;
    }
  }
  return -1;
}

module.exports = strictIndexOf;


/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

var baseIndexOf = __webpack_require__(324),
    isArrayLike = __webpack_require__(126),
    isString = __webpack_require__(344),
    toInteger = __webpack_require__(350),
    values = __webpack_require__(352);

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * Checks if `value` is in `collection`. If `collection` is a string, it's
 * checked for a substring of `value`, otherwise
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * is used for equality comparisons. If `fromIndex` is negative, it's used as
 * the offset from the end of `collection`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object|string} collection The collection to inspect.
 * @param {*} value The value to search for.
 * @param {number} [fromIndex=0] The index to search from.
 * @param- {Object} [guard] Enables use as an iteratee for methods like `_.reduce`.
 * @returns {boolean} Returns `true` if `value` is found, else `false`.
 * @example
 *
 * _.includes([1, 2, 3], 1);
 * // => true
 *
 * _.includes([1, 2, 3], 1, 2);
 * // => false
 *
 * _.includes({ 'a': 1, 'b': 2 }, 1);
 * // => true
 *
 * _.includes('abcd', 'bc');
 * // => true
 */
function includes(collection, value, fromIndex, guard) {
  collection = isArrayLike(collection) ? collection : values(collection);
  fromIndex = (fromIndex && !guard) ? toInteger(fromIndex) : 0;

  var length = collection.length;
  if (fromIndex < 0) {
    fromIndex = nativeMax(length + fromIndex, 0);
  }
  return isString(collection)
    ? (fromIndex <= length && collection.indexOf(value, fromIndex) > -1)
    : (!!length && baseIndexOf(collection, value, fromIndex) > -1);
}

module.exports = includes;


/***/ }),

/***/ 341:
/***/ (function(module, exports, __webpack_require__) {

var baseIsArguments = __webpack_require__(325),
    isObjectLike = __webpack_require__(53);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
  return isObjectLike(value) && hasOwnProperty.call(value, 'callee') &&
    !propertyIsEnumerable.call(value, 'callee');
};

module.exports = isArguments;


/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var root = __webpack_require__(124),
    stubFalse = __webpack_require__(348);

/** Detect free variable `exports`. */
var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse;

module.exports = isBuffer;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(129)(module)))

/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(52),
    isObject = __webpack_require__(128);

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(52),
    isArray = __webpack_require__(125),
    isObjectLike = __webpack_require__(53);

/** `Object#toString` result references. */
var stringTag = '[object String]';

/**
 * Checks if `value` is classified as a `String` primitive or object.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a string, else `false`.
 * @example
 *
 * _.isString('abc');
 * // => true
 *
 * _.isString(1);
 * // => false
 */
function isString(value) {
  return typeof value == 'string' ||
    (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag);
}

module.exports = isString;


/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(52),
    isObjectLike = __webpack_require__(53);

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;


/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

var baseIsTypedArray = __webpack_require__(327),
    baseUnary = __webpack_require__(330),
    nodeUtil = __webpack_require__(336);

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

module.exports = isTypedArray;


/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeKeys = __webpack_require__(321),
    baseKeys = __webpack_require__(328),
    isArrayLike = __webpack_require__(126);

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = keys;


/***/ }),

/***/ 348:
/***/ (function(module, exports) {

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

module.exports = stubFalse;


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

var toNumber = __webpack_require__(351);

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0,
    MAX_INTEGER = 1.7976931348623157e+308;

/**
 * Converts `value` to a finite number.
 *
 * @static
 * @memberOf _
 * @since 4.12.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {number} Returns the converted number.
 * @example
 *
 * _.toFinite(3.2);
 * // => 3.2
 *
 * _.toFinite(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toFinite(Infinity);
 * // => 1.7976931348623157e+308
 *
 * _.toFinite('3.2');
 * // => 3.2
 */
function toFinite(value) {
  if (!value) {
    return value === 0 ? value : 0;
  }
  value = toNumber(value);
  if (value === INFINITY || value === -INFINITY) {
    var sign = (value < 0 ? -1 : 1);
    return sign * MAX_INTEGER;
  }
  return value === value ? value : 0;
}

module.exports = toFinite;


/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

var toFinite = __webpack_require__(349);

/**
 * Converts `value` to an integer.
 *
 * **Note:** This method is loosely based on
 * [`ToInteger`](http://www.ecma-international.org/ecma-262/7.0/#sec-tointeger).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {number} Returns the converted integer.
 * @example
 *
 * _.toInteger(3.2);
 * // => 3
 *
 * _.toInteger(Number.MIN_VALUE);
 * // => 0
 *
 * _.toInteger(Infinity);
 * // => 1.7976931348623157e+308
 *
 * _.toInteger('3.2');
 * // => 3
 */
function toInteger(value) {
  var result = toFinite(value),
      remainder = result % 1;

  return result === result ? (remainder ? result - remainder : result) : 0;
}

module.exports = toInteger;


/***/ }),

/***/ 351:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(128),
    isSymbol = __webpack_require__(345);

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = toNumber;


/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var baseValues = __webpack_require__(331),
    keys = __webpack_require__(347);

/**
 * Creates an array of the own enumerable string keyed property values of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property values.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.values(new Foo);
 * // => [1, 2] (iteration order is not guaranteed)
 *
 * _.values('hi');
 * // => ['h', 'i']
 */
function values(object) {
  return object == null ? [] : baseValues(object, keys(object));
}

module.exports = values;


/***/ }),

/***/ 355:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(40);


/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(fetch) {

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.ev = exports.defaults = exports.StoreLocator = undefined;

var _includes3 = __webpack_require__(340);

var _includes4 = _interopRequireDefault(_includes3);

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
// import Notification from './notification';


var _mapConfig = __webpack_require__(133);

var _mapConfig2 = _interopRequireDefault(_mapConfig);

var _utils = __webpack_require__(41);

var _seamlessImmutable = __webpack_require__(93);

var _seamlessImmutable2 = _interopRequireDefault(_seamlessImmutable);

var _events = __webpack_require__(47);

var _events2 = _interopRequireDefault(_events);

var _store = __webpack_require__(137);

var _store2 = _interopRequireDefault(_store);

var _note = __webpack_require__(134);

var _note2 = _interopRequireDefault(_note);

var _storeAttribute = __webpack_require__(136);

var _storeAttribute2 = _interopRequireDefault(_storeAttribute);

var _settings2 = __webpack_require__(55);

var _progress = __webpack_require__(135);

var _progress2 = _interopRequireDefault(_progress);

var _infooverlay = __webpack_require__(132);

var _infooverlay2 = _interopRequireDefault(_infooverlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//let defaults = _merge({}, _settings);
var defaults = _seamlessImmutable2.default.merge({}, _settings2.defaults, { deep: true });

// this.rootEl = Emmet(el);
// this.el = this.rootEl.childNodes[0];

var StoreLocator = function () {
	function StoreLocator(props) {
		var _this = this;

		_classCallCheck(this, StoreLocator);

		//TODO: test if google api is pressent
		//TODO: deep assign
		exports.defaults = defaults = _seamlessImmutable2.default.merge(defaults, props, { deep: true });

		this.Progress = new _progress2.default({ message: defaults.labels.introText });
		this.Progress.start();

		//init state
		_events2.default.initState({
			selectedStore: null,
			activeFilters: [],
			activePanel: 'main',
			data: {},
			zoomLevel: defaults.mapOptions.zoom,
			notifications: [],
			location: null,
			geoLocation: false,
			filteredData: [],
			support: {
				transition: (0, _utils.featureTest)('transition')
			}
		});

		this._timeout = window.setTimeout;

		//check for smallscreen TODO build it more solid
		this.isSmallScreen = window.matchMedia ? window.matchMedia('(max-width: 750px)').matches : false;

		//html elements
		this.html = defaults.html.container instanceof Element ? defaults.html.container : document.querySelectorAll(defaults.html.container)[0];
		if (!this.html) {
			console.warn('Storelocator is missing a container');return;
		}
		this.listContainer = this.html.querySelectorAll(defaults.html.list)[0];
		this.list = this.listContainer.getElementsByTagName('ul')[0];
		this.input = this.html.querySelectorAll(defaults.html.input)[0];

		this.mainPanel = this.html.querySelectorAll(defaults.html.mainPanel)[0];

		//infooverlay
		this.infoOverlay = new _infooverlay2.default();

		//filter container
		this.filterAttributes = this.html.querySelectorAll(defaults.html.storeAttribute)[0];

		defaults.storeAttributes.forEach(function (attr) {
			_this.filterAttributes.appendChild(new _storeAttribute2.default(attr).el);
		});

		//injects map script in document
		this.injectMapsApi();

		// HTML Events

		_events2.default.addEvent(document, 'keydown', this.onKeyDownGlobal.bind(this), false);
		_events2.default.addEvent(document, 'click', this.onClickGlobal.bind(this), false);
		_events2.default.addEvent(this.html.querySelectorAll(defaults.html.geoLocation), 'click', this.onGeoLocationClick.bind(this), false);
		_events2.default.addEvent(this.html.querySelectorAll(defaults.html.searchButton), 'click', this.onSearchClick.bind(this), false);
		_events2.default.addEvent(this.html.querySelectorAll(defaults.html.panelController), 'click', this.onPanelControlClick.bind(this), false);

		// App events
		_events2.default.on(_settings2.ev.selectedStore, this.onSelectedStore.bind(this));
		_events2.default.on(_settings2.ev.showInfo, this.onShowInfo.bind(this));
		_events2.default.on(_settings2.ev.hideInfo, function () {
			_this.listContainer.classList.remove('no-overflow');
		});

		_events2.default.on(_settings2.ev.setFilter, this.filterChange.bind(this));
		_events2.default.on('state:changed', function (state) {
			//loggin all state changes
			console.log('state changed:', state);
		});
	}

	/*
 * inject maps api scriptblock
 * @Private
 */


	_createClass(StoreLocator, [{
		key: 'injectMapsApi',
		value: function injectMapsApi() {
			var mapsScript = document.createElement('script');
			var urlString = [];
			urlString.push(window.location.protocol);
			urlString.push(defaults.uri + '?');
			urlString.push('libraries=' + defaults.libraries.join(',') + '&');
			urlString.push('key=' + defaults.apiKey + '&');
			urlString.push('callback=' + defaults.callbackFn);
			mapsScript.setAttribute('src', urlString.join(''));
			mapsScript.setAttribute('async', '');
			document.head.appendChild(mapsScript);
			window[defaults.callbackFn] = this.initMap.bind(this);
			if (defaults.markerClusterer) {
				var clusterScript = document.createElement('script');
				clusterScript.setAttribute('src', '//developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js');
				clusterScript.setAttribute('async', '');
				document.head.appendChild(clusterScript);
			}
		}

		/******** Public functions *******/

		/*
  *  bounded to window object and called when map API is ready.
  *  @private
  */

	}, {
		key: 'initMap',
		value: function initMap() {
			var _this2 = this;

			var clientMapTypeId = 'client_style';
			var mapContainer = this.isSmallScreen && defaults.html.mapMobile ? this.html.querySelectorAll(defaults.html.mapMobile)[0] : this.html.querySelectorAll(defaults.html.map)[0];
			if (!mapContainer) {
				console.warn('Missing map container: ', mapContainer);
				return;
			}
			this.clientMapType = new google.maps.StyledMapType(_mapConfig2.default, { name: 'client_style' });

			this.map = new google.maps.Map(mapContainer, defaults.mapOptions);
			this.map.mapTypes.set(clientMapTypeId, this.clientMapType);

			google.maps.event.addListenerOnce(this.map, 'tilesloaded', function () {
				// Global ready event
				_events2.default.setState(_settings2.ev.mapReady, ['isSmallScreen'], _this2.isSmallScreen);
				// Load storedata
				_this2.getShops();
				// Fake loading delay
				setTimeout(function () {
					_this2.Progress.stop();
					_this2.html.classList.add(defaults.cssClass.ready);
				}, defaults.fakeLoadingDelay);
			});

			this.map.addListener('dragend', this.onMapDragend.bind(this));
			this.map.addListener('zoom_changed', this.onMapChange.bind(this));

			// Autocomplete
			this.autocomplete = new google.maps.places.Autocomplete(this.input);
			this.autocomplete.addListener('place_changed', this.onPlaceChange.bind(this));
		}

		/**
  *  Handles an autocomplete request for new place search
  *  @Private
  */

	}, {
		key: 'onPlaceChange',
		value: function onPlaceChange(place) {
			var _this3 = this;

			place = place || this.autocomplete.getPlace();
			if (_events2.default.state.location && this.locationMarker) {
				this.locationMarker.setMap(null);
				this.locationMarker = null;
			}
			if (place.geometry) {
				var icon = new google.maps.MarkerImage(defaults.icons.places.url, null, null, null, new google.maps.Size(defaults.icons.places.w, defaults.icons.places.h));
				this.locationMarker = this.marker = new google.maps.Marker({
					position: place.geometry.location,
					map: this.map,
					icon: icon,
					clickable: true,
					draggable: true,
					animation: google.maps.Animation.DROP

				});
				this.locationMarker.setAnimation(google.maps.Animation.BOUNCE);
				window.setTimeout(function () {
					_this3.locationMarker.setAnimation(null);
				}, defaults.bounceTimeout);

				_events2.default.setState(_settings2.ev.placeChange, ['location'], place.formatted_address); // jshint ignore:line

				if (place.geometry.viewport) {
					this.map.fitBounds(place.geometry.viewport);
				} else {
					this.map.setCenter(place.geometry.location);
					this.map.setZoom(17); // Why 17? Because it looks good.
				}
			}
		}

		/**
  *  Intialize shop list. This function intializes all store intances from first fetch. should only be called once
  *  @public
  *  @param {Array} data - Array with stores to be added to the map. The struckture is documented in readme.md
  */

	}, {
		key: 'setStoreData',
		value: function setStoreData(data) {
			// data is comming from an external service executed outside of StoreLocator class.
			var filteredData = [];
			this.stores = [];
			_events2.default.setState(_settings2.ev.setStoreData, ['data'], data, true);

			if (defaults.mapData.initFilter) {
				filteredData = data.filter(defaults.mapData.initFilter);
			} else {
				filteredData = data;
			}
			filteredData = this.setDistanceToCenter(filteredData);
			this.createStores(filteredData);
			this.setListSize();

			//clusters
			// this.markerCluster = new MarkerClusterer(this.map, this.markers, {
			//  	imagePath: defaults.icons.cluster.url,
			//  	setMaxZoom: defaults.maxZoomLevelClusterer
			// });
		}

		/**
  *  createStores - main update step in the application it creates stores and reRender stores based on the filterdData provided
  * @param { Array } - array of data to publish to UI
  */

	}, {
		key: 'createStores',
		value: function createStores(filteredData) {
			var _this4 = this;

			//var stores = Events.get(['stores']);
			//destroy existing stores

			var cachedStores = [];
			this.stores.forEach(function (store) {
				if (_this4.isStoreInBound(store.data)) {
					//already exist or under a filter  removed(|| selectedStoreId === store.data.id)
					cachedStores.push(store);
				} else {
					store.destroy();
				}
			});
			//filter
			if (_events2.default.state.activeFilters.length) {
				cachedStores.forEach(function (store) {
					if (!_this4.filterStoreAttribute(store.data, 'sid')) {
						store.destroy();
					}
				});
			}

			this.stores = filteredData.map(function (storeModel) {
				//create new stores and makers
				var thisIsCachedStore = cachedStores.find(function (item) {
					return item.data.id === storeModel.id && item.marker;
				});
				if (cachedStores.length && thisIsCachedStore) {
					// performance
					thisIsCachedStore.reRender(storeModel);
					_this4.list.appendChild(thisIsCachedStore.html());
					return thisIsCachedStore;
				} else {
					var item = new _store2.default(storeModel, _this4.map);
					_this4.list.appendChild(item.html());
					return item;
				}
			});

			_events2.default.setState('app:updatefiltereddata', ['filteredData'], filteredData);
			this.markers = this.getMarkers();
		}

		/****** Private methods ********/

		/**
  *  onSetSelected
  */

	}, {
		key: 'onSelectedStore',
		value: function onSelectedStore(state) {
			this.selectedStore = this.stores.find(function (store) {
				return store.data.id === state.selectedStore;
			});
			this.getStoresInBound();
		}

		/*
  * get markers available
  * @private
  *
  */

	}, {
		key: 'getMarkers',
		value: function getMarkers() {
			return this.stores.map(function (store) {
				return store.marker;
			});
		}

		/****** Private event handlers ********/

		/**
  *  set number of stores text in UI
  */

	}, {
		key: 'setListSize',
		value: function setListSize() {
			this.html.querySelectorAll(defaults.html.listSize)[0].textContent = this.stores.length;
		}

		/*
  * onMapChange - zoom and pan eventHandler
  *
  */

	}, {
		key: 'onMapChange',
		value: function onMapChange() {

			var zoom = this.map.getZoom();
			if (zoom > defaults.zoomLevelFilterStoresInBound) {
				this.getStoresInBound();
			}
			_events2.default.setState(_settings2.ev.zoom, ['zoom'], zoom);
		}

		/**
  *  onMapDragend
  */

	}, {
		key: 'onMapDragend',
		value: function onMapDragend() {
			if (this.map.getZoom() > defaults.zoomLevelFilterStoresInBound) {
				this.getStoresInBound();
			}
		}

		/**
  *  onShowInfo
  */

	}, {
		key: 'onShowInfo',
		value: function onShowInfo(state) {
			var _this5 = this;

			this.getStoreDetails(state.openInfoOverlay).then(function (detailModel) {
				_this5.infoOverlay.reRender(detailModel);
				if (_this5.isSmallScreen) {
					_this5.html.appendChild(_this5.infoOverlay.html());
				} else {
					_this5.html.querySelectorAll(defaults.html.sidebar)[0].appendChild(_this5.infoOverlay.html());
				}
				_this5.infoOverlay.setActive();
				window.setTimeout(function () {
					_this5.infoOverlay.el.classList.add(defaults.transitionIn);
				}, 50);
			});
		}

		/**
  *  setDistanceToCenter
  */

	}, {
		key: 'setDistanceToCenter',
		value: function setDistanceToCenter(collection) {
			var center = this.map.getCenter();
			collection = [].concat(collection);
			collection = collection.map(function (item) {
				var markerPos = new google.maps.LatLng(item.latitude, item.longitude);
				item = _seamlessImmutable2.default.asMutable(item);
				item.distanceToCenter = google.maps.geometry.spherical.computeDistanceBetween(center, markerPos);
				return item;
			});
			collection.sort(function (a, b) {
				return parseFloat(a.distanceToCenter) - parseFloat(b.distanceToCenter);
			});
			return collection;
		}

		/**
  *  setDistanceToGeoLocation
  */

	}, {
		key: 'setDistanceToGeoLocation',
		value: function setDistanceToGeoLocation(collection) {
			if (!_events2.default.state.geoLocation) return false;

			collection = collection.map(function (item) {
				var markerPos = new google.maps.LatLng(item.latitude, item.longitude);
				item = _seamlessImmutable2.default.asMutable(item);
				item.distanceToGeoLocation = Math.round(google.maps.geometry.spherical.computeDistanceBetween(_events2.default.state.geoLocation, markerPos) / 1000);
				return item;
			});
			return collection;
		}

		/**
  *  getStoreDetails
  */

	}, {
		key: 'getStoreDetails',
		value: function getStoreDetails(id) {
			if (defaults.services.details) {
				var url = defaults.services.details + '?' + (0, _utils.queryParam)({ 'id': id });
				return fetch(url, {
					method: 'GET'
				}).then(function (res) {
					return res.json();
				});
			} else {
				var _ret = function () {
					var data = _events2.default.state.filteredData;
					return {
						v: new Promise(function (resolve, reject) {
							var shop = data.filter(function (item) {
								return item.id === id;
							});
							resolve(shop[0]);
							reject('Error: no local data found');
						})
					};
				}();

				if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
			}
		}

		/**
  *  getStoresInBound
  */

	}, {
		key: 'getStoresInBound',
		value: function getStoresInBound() {
			var _this6 = this;

			var filteredData;
			var data = this.getData();
			// get stores currently active on the map
			filteredData = data.filter(function (storeModel) {
				return _this6.isStoreInBound(storeModel);
			});
			// filter from filters
			if (_events2.default.state.activeFilters.length) {
				filteredData = filteredData.filter(function (storeModel) {
					return _this6.filterStoreAttribute(storeModel, 'sid');
				});
			}
			if (!filteredData.length) {
				// there are no stores in the filtered data find the nearst store
				this.noStoresInBound();
				//Events.emit(ev.noStoresInBound);
			} else {
				filteredData = this.setDistanceToCenter(filteredData);
				filteredData = _events2.default.state.geoLocation ? this.setDistanceToGeoLocation(filteredData) : filteredData;
			}
			this.createStores(filteredData);
			this.setListSize();
		}

		/**
  *  filterStore
  */

	}, {
		key: 'filterStoreAttribute',
		value: function filterStoreAttribute(storeModel, key) {
			return (0, _includes4.default)(_events2.default.state.activeFilters, storeModel[key]);
		}

		/**
  *  isStoreInBound
  */

	}, {
		key: 'isStoreInBound',
		value: function isStoreInBound(storeModel) {
			var mapBounds = this.map.getBounds();
			return mapBounds.contains({ lat: storeModel.latitude, lng: storeModel.longitude });
		}

		/**
  * get data from data service. TODO: need to be
  * @public
  */

	}, {
		key: 'getData',
		value: function getData() {
			return _events2.default.state.data;
		}
	}, {
		key: 'getShops',
		value: function getShops() {
			var _this7 = this;

			fetch(defaults.services.shops).then(function (res) {
				return res.json();
			}).then(function (data) {
				//var filteredData = data.filter(defaults.mapData.initFilter)
				data = defaults.apiDataKey ? data[defaults.apiDataKey] : data;
				var transformedData = data.map(defaults.mapData.storeModel);
				_this7.setStoreData(transformedData);
			});
		}

		/**
  *  getLocationFromAddress
  */

	}, {
		key: 'getLocationFromAddress',
		value: function getLocationFromAddress(address) {
			var geocoder = new google.maps.Geocoder();
			return new Promise(function (resolve, reject) {
				geocoder.geocode({ 'address': address }, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						resolve(results);
					} else {
						reject('Geocode was not successful: ' + status);
					}
				});
			});
		}

		/**
  *  pandelControl handler
  */

	}, {
		key: 'onPanelControlClick',
		value: function onPanelControlClick(e) {
			var _this8 = this;

			e.preventDefault();
			var target = e.currentTarget;
			var selectedPanel = this.getHashFromEl(target);
			if (!selectedPanel || !this.html.querySelectorAll('#' + selectedPanel).length) {
				console.warn('No panel found with the #id: ', selectedPanel);
				return;
			}
			(0, _utils.each)(this.html.querySelectorAll('.panel'), function (el) {
				el.classList.toggle('is-active', el.id === selectedPanel);
			});
			(0, _utils.each)(this.html.querySelectorAll(defaults.html.panelController), function (el) {
				var selectedButton = _this8.getHashFromEl(el);
				el.classList.toggle('is-active', selectedButton === selectedPanel);
			});
			_events2.default.setState(_settings2.ev.panelChange, ['activePanel'], selectedPanel);
		}

		/**
  *  getHashFromEl
  */

	}, {
		key: 'getHashFromEl',
		value: function getHashFromEl(el) {
			var result = el.hash && el.tagName.toLowerCase() === 'a' ? el.hash : el.getAttribute('data-href');
			return result.replace('#', '');
		}

		/**
  *  onGeoLocationClick
  */

	}, {
		key: 'onGeoLocationClick',
		value: function onGeoLocationClick(e) {
			var _this9 = this;

			e.preventDefault();
			this.Progress.start({ message: defaults.labels.getGeoLocation });
			if (this.geoLocationMarker) {
				this.geoLocationMarker.setMap(null);
				this.geoLocationMarker = null;
			}

			this.getUserCords().then(this.handleGeoLocation.bind(this), function (error) {
				_events2.default.setState(_settings2.ev.setLocation, ['geoLocation'], false);
				_this9.Progress.stop();
				console.warn('GeoLocation: ', error);
			});
		}

		/**
  *  getUserCords
  *  @private
  */

	}, {
		key: 'getUserCords',
		value: function getUserCords() {
			return new Promise(function (resolve, reject) {
				if (navigator.permissions) {
					navigator.permissions.query({ name: 'geolocation' }).then(function (permission) {
						if (permission.state === 'granted' || 'prompt') {
							navigator.geolocation.getCurrentPosition(function (pos) {
								return resolve(pos);
							}, reject, { enableHighAccuracy: true });
						} else {
							resolve(null);
							reject();
						}
					});
				} else {
					reject(new Error('Permission API is not supported'));
				}
			});
		}

		/**
  *  onNoStoresInBound TODO: handle this situation
  */

	}, {
		key: 'noStoresInBound',
		value: function noStoresInBound() {
			var _this10 = this;

			if (this.note) this.note.destroy();
			var data = this.getData();
			var store = this.setDistanceToCenter(data)[0];
			this.note = new _note2.default({
				title: defaults.labels.noStoresFound.title,
				subTitle: defaults.labels.noStoresFound.subTitle,
				button: {
					text: defaults.labels.noStoresFound.button
				},
				onClick: function onClick(e) {
					e.preventDefault();
					_this10.expandBound(store);
					_this10.note.destroy();
				}
			});

			this.list.appendChild(this.note.html());
		}

		/**
  *  handleGeoLocation
  */

	}, {
		key: 'handleGeoLocation',
		value: function handleGeoLocation(position) {
			this.Progress.stop();
			var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			_events2.default.setState(_settings2.ev.setLocation, ['geoLocation'], latlng);
			var icon = new google.maps.MarkerImage(defaults.icons.geolocation.url, null, null, null, new google.maps.Size(defaults.icons.geolocation.w, defaults.icons.geolocation.h));
			this.geoLocationMarker = new google.maps.Marker({
				position: latlng,
				map: this.map,
				title: defaults.labels.youAreHere,
				icon: icon,
				draggable: true,
				client_id: 'you' // jshint ignore:line
			});
			this.map.setCenter(latlng);
			this.map.setZoom(12);
			this.getStoresInBound();
		}

		/**
  *  expandBound - takes a storeModel and expands the bound to contain the provided store
  */

	}, {
		key: 'expandBound',
		value: function expandBound(storeModel) {
			var offset = 0.02;
			var bounds = this.map.getBounds();
			var pos = new google.maps.LatLng(storeModel.latitude - offset, storeModel.longitude - offset);
			bounds.extend(pos);
			this.map.setCenter(bounds.getCenter());
			this.map.fitBounds(bounds);
			this.getStoresInBound();
		}
		/**
  * onClick - global accessibility helper
  *
  */

	}, {
		key: 'onClickGlobal',
		value: function onClickGlobal() {
			//remove accessible helper
			document.body.removeAttribute('data-focus-source');
		}

		/**
  * on change handler to the filter functionality
  *
  */

	}, {
		key: 'filterChange',
		value: function filterChange() {
			this.getStoresInBound();
		}

		/**
  * keyDownGlobal
  *
  */

	}, {
		key: 'onKeyDownGlobal',
		value: function onKeyDownGlobal(e) {
			//accessibility focus helper
			if (e.keyCode === _settings2.ev.keyCode.tab) {
				document.body.setAttribute('data-focus-source', 'keyboard');
			}
		}
		/**
  *  onSearchClick
  */

	}, {
		key: 'onSearchClick',
		value: function onSearchClick(e) {
			var _this11 = this;

			e.preventDefault();
			var service = new google.maps.places.AutocompleteService();
			service.getPlacePredictions({ input: this.input.value }, function (result) {
				_this11.getLocationFromAddress(result[0].description).then(function (place) {
					_this11.onPlaceChange(place[0]);
					_this11.input.value = place[0].formatted_address; // jshint ignore:line
				});
			});
		}
	}], [{
		key: 'events',
		get: function get() {
			return _events2.default;
		}
	}]);

	return StoreLocator;
}();

exports.StoreLocator = StoreLocator;
exports.defaults = defaults;
exports.ev = _settings2.ev;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(320)))

/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

// Tagged template function
function formatSelector(selector) {
	if (!selector) return;
	return selector.replace(/[.,\/#\[\]!$%\^&\*;:{}=\\]/g, '');
}
function find(arg0, arg1) {
	var result;
	if (typeof arg1 == 'undefined') {
		result = document.querySelectorAll(arg0);
	} else {
		result = arg0.querySelectorAll(arg1);
	}
	return result;
}
function each(nodeList, cb) {
	for (var i = 0; i < nodeList.length; i++) {
		cb(nodeList[i], i);
	}
}
function queryParam(obj) {
	var esc = encodeURIComponent;
	var query = Object.keys(obj).map(function (k) {
		return esc(k) + '=' + esc(obj[k]);
	});
	return query.join('&');
}
function featureTest(feat) {
	var b = document.body || document.documentElement,
	    s = b.style,
	    p = feat;

	if (typeof s[p] == 'string') {
		return true;
	}

	// Tests for vendor specific prop
	var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
	p = p.charAt(0).toUpperCase() + p.substr(1);

	for (var i = 0; i < v.length; i++) {
		if (typeof s[v[i] + p] == 'string') {
			return true;
		}
	}
	return false;
}

exports.formatSelector = formatSelector;
exports.find = find;
exports.each = each;
exports.queryParam = queryParam;
exports.featureTest = featureTest;

/***/ }),

/***/ 47:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _eventEmitterEs = __webpack_require__(319);

var _eventEmitterEs2 = _interopRequireDefault(_eventEmitterEs);

var _seamlessImmutable = __webpack_require__(93);

var _seamlessImmutable2 = _interopRequireDefault(_seamlessImmutable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } //https://www.npmjs.com/package/es6-event-emitter


var Events = function (_EventEmitter) {
	_inherits(Events, _EventEmitter);

	function Events() {
		_classCallCheck(this, Events);

		var _this = _possibleConstructorReturn(this, (Events.__proto__ || Object.getPrototypeOf(Events)).call(this));

		_this.hasEventListeners = !!window.addEventListener;
		_this.state = {};

		return _this;
	}

	_createClass(Events, [{
		key: 'addEvent',
		value: function addEvent(el, e, callback, capture) {
			var _this2 = this;

			var els = el instanceof Element ? [el] : el;
			[].forEach.call(els, function (el) {
				if (_this2.hasEventListeners) {
					el.addEventListener(e, callback, !!capture);
				} else {
					el.attachEvent('on' + e, callback);
				}
			});
		}
	}, {
		key: 'removeEvent',
		value: function removeEvent(el, e, callback, capture) {
			var _this3 = this;

			var els = el instanceof Element ? [el] : el;
			[].forEach.call(els, function (el) {
				if (_this3.hasEventListeners) {
					el.removeEventListener(e, callback, !!capture);
				} else {
					el.detachEvent('on' + e, callback);
				}
			});
		}
	}, {
		key: 'trigger',
		value: function trigger(el, eventName, detail) {
			var e;
			if (window.CustomEvent) {
				e = new CustomEvent(eventName, { bubbles: true, cancelable: true, detail: detail });
			} else {
				e = document.createEvent('Event');
				e.initEvent(eventName, true, true, { detail: detail });
				e.detail = detail;
			}
			el.dispatchEvent(event);
		}
	}, {
		key: 'initState',
		value: function initState(state) {
			this.state = _seamlessImmutable2.default.from(state);
			this.stateActivated = true;
		}

		/**
  * mergeState is an immutable helper that merge a state object. returns this new state.
  * @param eventName {String} - a static eventName to emit immitialty after the state is updated
  * @param key {Array} - keys to select
  * @param newVal {all} - new value to set in Immutable state object
  * @param deep {Bool} - performs a deep compare (default to false)
  */

	}, {
		key: 'mergeState',
		value: function mergeState(eventName, newState, deep) {
			this.state = _seamlessImmutable2.default.merge(this.state, newState, { deep: deep });
			this.emit('state:changed', this.state);
			this.emit(eventName, this.state);
			return this.state;
		}

		/**
  * setState is an immutable helper that set a state in a particular place of the object. returns this new state.
  * @param eventName {String} - a static eventName to emit immitialty after the state is updated
  * @param key {Array} - keys to select
  * @param newVal {all} - new value to set in Immutable state object
  * @param deep {Bool} - performs a deep compare (default to false)
  */

	}, {
		key: 'setState',
		value: function setState(eventName, key, newVal, deep) {
			deep = deep || false;
			this.state = _seamlessImmutable2.default.setIn(this.state, key, newVal, { deep: deep });
			this.emit('state:changed', this.state);
			this.emit(eventName, this.state);
			return this.state;
		}
	}, {
		key: 'getState',
		value: function getState(query, defaultVal) {
			defaultVal = defaultVal || null;
			return _seamlessImmutable2.default.getIn(this.state, query);
		}
	}]);

	return Events;
}(_eventEmitterEs2.default);

exports.default = new Events();

/***/ }),

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _events = __webpack_require__(47);

var _events2 = _interopRequireDefault(_events);

var _utils = __webpack_require__(41);

var _seamlessImmutable = __webpack_require__(93);

var _seamlessImmutable2 = _interopRequireDefault(_seamlessImmutable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MicroTemplate = function () {
	function MicroTemplate(data) {
		_classCallCheck(this, MicroTemplate);

		this.data = data;
		this.eventListeners = [];
		var root = document.createDocumentFragment(),
		    el = document.createElement('span');
		root.appendChild(el);
		this.rootEl = root.childNodes[0]; //span: used as container for rendering template
		this.preRender(data);
	}

	_createClass(MicroTemplate, [{
		key: 'delegateEvents',
		value: function delegateEvents(events) {
			var _this = this;

			if (this.eventListeners.length > 0) throw new Error('Event listeners have already been delegated!');
			events = _seamlessImmutable2.default.merge({
				'transitionend': 'onTransitionEnd'
			}, events);
			this.events = events;

			var _loop = function _loop(prop) {
				var eventSplit = prop.split(' ');
				var eventName = eventSplit[0];

				// is the target specified with a selector, or is this.el implied?
				var target = eventSplit.length > 1 ? eventSplit[1] : _this.el;
				if (!_this[events[prop]]) {
					throw new Error('MicroTemplate is missing valid eventhandler', _this);
				}
				var eventHandler = _this[events[prop]].bind(_this);

				// is the target already an element or a selector?
				var elements = void 0;
				if (typeof target == 'string') {
					elements = (0, _utils.find)(_this.el, target);
				} else if (target.length && target != _this.el) {
					elements = (0, _utils.find)(_this.el, target[0]);
				} else {
					elements = [_this.el];
				}
				(0, _utils.each)(elements, function (element) {
					/*jshint ignore:line */
					_this.eventListeners.push({ element: element, eventName: eventName, eventHandler: eventHandler });
					_events2.default.addEvent(element, eventName, eventHandler, false);
				});
			};

			for (var prop in events) {
				_loop(prop);
			}
		}
	}, {
		key: 'undelegateEvents',
		value: function undelegateEvents() {
			this.eventListeners.forEach(function (listener) {
				_events2.default.removeEvent(listener.element, listener.eventName, listener.eventHandler);
			});
			this.eventListeners = [];
			return true;
		}
	}, {
		key: 'onTransitionEnd',
		value: function onTransitionEnd() {
			return;
		}

		//TODO: check validity of html before letting the browser parse it.
		/*
  *  @private
  */

	}, {
		key: 'preRender',
		value: function preRender(data) {
			data = data || {};
			var el = this.render(data);
			if (!el) el = '<span></span>';
			if (!this.isValidHtml(el)) console.warn('Mirotemplate - Somethings wrong with the markup, typically a missing quote..');
			this.rootEl.innerHTML = el;
			this.el = this.rootEl.childNodes[0];
			this.afterRender();
		}
		/*
  * @private
  */

	}, {
		key: 'isValidHtml',
		value: function isValidHtml(str) {
			//if((str.match(/\"/g) || []).length%2 != 0) return false;
			var doc = new DOMParser().parseFromString(str, 'text/html');
			return Array.from(doc.body.childNodes).some(function (node) {
				if (node.nodeType != 1) console.warn('Microtemplate - try to find this node', node);
				return node.nodeType === 1;
			});
		}
		/*
  *  @private
  */

	}, {
		key: 'destroyElement',
		value: function destroyElement() {
			if (this.undelegateEvents()) {
				this.el.classList.add('off');
				if (this.el.remove) {
					this.el.remove();
				} else {
					this.el.parentNode.removeChild(this.el);
				}
			}
		}

		/*
  * @public
  */

	}, {
		key: 'html',
		value: function html() {
			this.isRendered = true;
			return this.el;
		}

		/*
  * @public
  */

	}, {
		key: 'reRender',
		value: function reRender(data) {
			this.destroyElement();
			this.preRender(data);
			this.delegateEvents(this.events);
			this.afterReRender();
		}
		/*
  * @public
  */

	}, {
		key: 'afterRender',
		value: function afterRender() {
			return;
		}
		/*
  * @public
  */

	}, {
		key: 'afterReRender',
		value: function afterReRender() {
			return;
		}
		/*
  * @public
  */

	}, {
		key: 'render',
		value: function render() {
			console.warn('no markup defined for microcomponent');
		}
		/*
  *  @public
  */

	}, {
		key: 'destroy',
		value: function destroy() {
			this.destroyElement();
		}
	}]);

	return MicroTemplate;
}();

exports.default = MicroTemplate;

/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(122),
    getRawTag = __webpack_require__(332),
    objectToString = __webpack_require__(337);

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),

/***/ 53:
/***/ (function(module, exports) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),

/***/ 54:
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 55:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var defaults = {
	"uri": "//maps.googleapis.com/maps/api/js",
	"libraries": ['places', 'geometry'],
	"callbackFn": "initClientMap",
	"useScriptInject": true,
	"zoomLevelOnListClick": 17,
	"countryFilterOnInitialize": null,
	"countryFilterOnInitializeKey": null,
	"markerClusterer": true,
	"markerClustererStart": 8,
	"maxZoomLevelClusterer": 8,
	"askForGeoLocation": null,
	"fakeLoadingDelay": 0,
	"zoomLevelFilterStoresInBound": 7,
	"bounceTimeout": 2000,
	"animationDuration": 200,
	"transitionIn": "on",
	"transitionOut": "off",
	"spinnerUrl": "/static/puff.svg",
	"apiDataKey": null,
	"html": {
		"container": ".js-storelocator",
		"map": "[data-sl-map]",
		"sidebar": "[data-sl-sidebar]",
		"mapMobile": "[data-sl-mapmobile]",
		"list": "[data-sl-list]",
		"listItem": "[data-sl-listitem]",
		"listSize": "[data-sl-listsize]",
		"infoItem": "[data-sl-info]",
		"input": "[data-sl-input]",
		"searchButton": "[data-sl-placesearch]",
		"clearButton": "[data-sl-placeclear]",
		"filter": "[data-sl-filter]",
		"filterButton": "[data-sl-filterbutton]",
		"mainPanel": "[data-sl-mainpanel]",
		"geoLocation": "[data-sl-geolocation]",
		"overlayClose": "[data-sl-overlayclose]",
		"infoBox": "[data-el-infobox]",
		"storeAttribute": "[data-sl-storeattribute]",
		"panelController": "[data-sl-panelcontroller]",
		"storeMoreInfo": "[data-sl-storemoreinfo]"

	},
	"cssClass": {
		"isActive": "is-active",
		"isHover": "is-hover",
		"isHidden": "is-hidden",
		"isSelected": "is-selected",
		"visibilityHidden": 'visibility-hidden',
		"ready": "storelocator--ready",
		"storeItem": "storelocator__list-item",
		"storeItemInner": "storelocator__list-item-inner",
		"infoOverlay": "storelocator__info-overlay",
		"infoOverlayClose": "storelocator__info-overlay-close",
		"infoOverlayContent": "storelocator__info-overlay-content",
		"infoAddrHeader": "info-address__header",
		"infoSection": "info-section",
		"infoLinklist": "info-section__linklist",
		"infoAddress": "info-section__address",
		"infoHours": "info-section__hours",
		"progress": "storelocator-progress",
		"progressMsg": "storelocator-progress__message",
		"note": "storelocator-note",
		"noteSubTitle": "storelocator-note__subtitle"

	},
	"storeAttributes": [{
		"sid": "S1",
		"name": "Bang & Olufsen Stores",
		"markerIcon": {
			"url": "/static/beo-store-round.svg",
			"w": 40,
			"h": 40
		},
		"markerIconSelected": {
			"url": "/static/authorised-retailers-round.svg",
			"w": 40,
			"h": 40
		},
		"filterIcon": "/static/beo-store.svg",
		"weight": 2
	}, {
		"sid": "S2",
		"name": "Authorised Retailers",
		"markerIcon": {
			"url": "/static/authorised-retailers-round.svg",
			"w": 40,
			"h": 40
		},
		"markerIconSelected": {
			"url": "/static/beo-store-round.svg",
			"w": 40,
			"h": 40
		},
		"filterIcon": "/static/authorised-retailers.svg",
		"weight": 2
	}, {
		"sid": "S3",
		"name": "Other Dealers",
		"markerIcon": {
			"url": "/static/other-dealers-round.svg",
			"w": 40,
			"h": 40
		},
		"markerIconSelected": {
			"url": "/static/beo-store-round.svg",
			"w": 40,
			"h": 40
		},
		"filterIcon": "/static/other-dealers.svg",
		"weight": 3
	}],
	"icons": {
		"selected": { "url": "/static/marker-selected.svg", "w": 31, "h": 43 },
		"hover": { "url": "/static/marker-hover.svg", "w": 31, "h": 43 },
		"default": { "url": "/static/marker-default.svg", "w": 20, "h": 20 },
		"geolocation": { "url": "/static/geolocation.svg", "w": 24, "h": 39 },
		"places": { "url": "/static/marker-places.svg", "w": 20, "h": 20 },
		"cluster": { "url": "//developers.google.com/maps/documentation/javascript/examples/markerclusterer/m" },
		"info": { "url": "/static/info.svg" }

	},
	"mapOptions": {
		"mapTypeId": "client_style",
		"zoom": 7,
		"center": {
			"lat": 55.88,
			"lng": 9.72
		},
		"mapTypeControl": false
	},
	"services": {
		"stores": null,
		"details": null
	},
	"templates": {},

	"mapData": {
		"storeModel": function storeModel(item) {
			return {
				address1: item.Address1,
				address2: item.Address3,
				zipCode: item.ZipCode,
				city: item.City,
				latitude: item.Latitude,
				longitude: item.Longitude,
				id: item.ID,
				sid: item.RetailerTypeID,
				countryCode: item.CountryISO,
				openingHours: Object.keys(item.OpeningHours).map(function (key) {
					return { label: key, hours: item.OpeningHours[key] };
				}),
				links: [{
					label: "",
					text: "Website",
					href: item.HomepageUrl
				}, {
					label: "Mail: ",
					text: item.Email,
					href: 'mailto:' + item.Email
				}, {
					label: "Phone: ",
					text: item.Phone,
					href: 'tel:' + item.Phone
				}]
			};
		},
		"detailModel": function detailModel(item) {
			return {
				address1: item.Address1,
				address2: item.Address3,
				zipCode: item.ZipCode,
				city: item.City,
				distance: null,
				openingHours: Object.keys(item.OpeningHours).map(function (key) {
					return { label: key, hours: item.OpeningHours[key] };
				}),
				links: [{
					label: "Website",
					title: "website",
					href: item.HomepageUrl
				}, {
					label: "Mail",
					title: "mail",
					href: item.Email
				}, {
					label: "phone",
					title: "phone",
					href: 'tel:' + item.Phone
				}]
			};
		}
	},
	"labels": {
		"introText": "Map is been build -please wait a sec...",
		"getGeoLocation": "We are trying to find u'",
		"noStoresInBound": "We couldn't find a store in this area, found the nearst for you",
		"distance": "away from you",
		"distanceUnit": "km.",
		"overlayClose": 'Close',
		"moreInfo": 'Mere info',
		"youAreHere": 'Din placering',
		"noStoresFound": {
			"title": 'Der er ingen butikker',
			"subtitle": 'Vi fandt desværre ingen butikker, du kan udvide søgningen eller finde en butik nær dig.',
			"button": 'Udvid søgning'
		}

	}
};

//Internal event strings
var ev = {
	stateChanged: 'state:changed',
	mapReady: 'map:ready',
	markerClick: 'marker:click',
	listItemClick: 'store:click',
	setSelected: 'store:setselected',
	selectedStore: 'store:selectedstore',
	clearList: 'map:clearlist',
	noStoresInBound: 'map:nostoresinbound',
	showInfo: 'store:showinfo',
	hideInfo: 'inforoverlay:close',
	setFilter: 'storeattribute:select',
	setStoreData: 'app:setstoredata',
	setLocation: 'places:location',
	panelChange: 'panels:change',
	keyCode: {
		tab: 9
	}
};

exports.ev = ev;
exports.defaults = defaults;

/***/ }),

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;(function() {
  "use strict";

function immutableInit(config) {

  // https://github.com/facebook/react/blob/v15.0.1/src/isomorphic/classic/element/ReactElement.js#L21
  var REACT_ELEMENT_TYPE = typeof Symbol === 'function' && Symbol.for && Symbol.for('react.element');
  var REACT_ELEMENT_TYPE_FALLBACK = 0xeac7;

  var globalConfig = {
    use_static: false
  };
  if (isObject(config)) {
      if (config.use_static !== undefined) {
          globalConfig.use_static = Boolean(config.use_static);
      }
  }

  function isObject(data) {
    return (
      typeof data === 'object' &&
      !Array.isArray(data) &&
      data !== null
    );
  }

  function instantiateEmptyObject(obj) {
      var prototype = Object.getPrototypeOf(obj);
      if (!prototype) {
          return {};
      } else {
          return Object.create(prototype);
      }
  }

  function addPropertyTo(target, methodName, value) {
    Object.defineProperty(target, methodName, {
      enumerable: false,
      configurable: false,
      writable: false,
      value: value
    });
  }

  function banProperty(target, methodName) {
    addPropertyTo(target, methodName, function() {
      throw new ImmutableError("The " + methodName +
        " method cannot be invoked on an Immutable data structure.");
    });
  }

  var immutabilityTag = "__immutable_invariants_hold";

  function addImmutabilityTag(target) {
    addPropertyTo(target, immutabilityTag, true);
  }

  function isImmutable(target) {
    if (typeof target === "object") {
      return target === null || Boolean(
        Object.getOwnPropertyDescriptor(target, immutabilityTag)
      );
    } else {
      // In JavaScript, only objects are even potentially mutable.
      // strings, numbers, null, and undefined are all naturally immutable.
      return true;
    }
  }

  function isEqual(a, b) {
    // Avoid false positives due to (NaN !== NaN) evaluating to true
    return (a === b || (a !== a && b !== b));
  }

  function isMergableObject(target) {
    return target !== null && typeof target === "object" && !(Array.isArray(target)) && !(target instanceof Date);
  }

  var mutatingObjectMethods = [
    "setPrototypeOf"
  ];

  var nonMutatingObjectMethods = [
    "keys"
  ];

  var mutatingArrayMethods = mutatingObjectMethods.concat([
    "push", "pop", "sort", "splice", "shift", "unshift", "reverse"
  ]);

  var nonMutatingArrayMethods = nonMutatingObjectMethods.concat([
    "map", "filter", "slice", "concat", "reduce", "reduceRight"
  ]);

  var mutatingDateMethods = mutatingObjectMethods.concat([
    "setDate", "setFullYear", "setHours", "setMilliseconds", "setMinutes", "setMonth", "setSeconds",
    "setTime", "setUTCDate", "setUTCFullYear", "setUTCHours", "setUTCMilliseconds", "setUTCMinutes",
    "setUTCMonth", "setUTCSeconds", "setYear"
  ]);

  function ImmutableError(message) {
    var err       = new Error(message);
    // TODO: Consider `Object.setPrototypeOf(err, ImmutableError);`
    err.__proto__ = ImmutableError;

    return err;
  }
  ImmutableError.prototype = Error.prototype;

  function makeImmutable(obj, bannedMethods) {
    // Tag it so we can quickly tell it's immutable later.
    addImmutabilityTag(obj);

    if (true) {
      // Make all mutating methods throw exceptions.
      for (var index in bannedMethods) {
        if (bannedMethods.hasOwnProperty(index)) {
          banProperty(obj, bannedMethods[index]);
        }
      }

      // Freeze it and return it.
      Object.freeze(obj);
    }

    return obj;
  }

  function makeMethodReturnImmutable(obj, methodName) {
    var currentMethod = obj[methodName];

    addPropertyTo(obj, methodName, function() {
      return Immutable(currentMethod.apply(obj, arguments));
    });
  }

  function arraySet(idx, value, config) {
    var deep          = config && config.deep;

    if (idx in this) {
      if (deep && this[idx] !== value && isMergableObject(value) && isMergableObject(this[idx])) {
        value = Immutable.merge(this[idx], value, {deep: true, mode: 'replace'});
      }
      if (isEqual(this[idx], value)) {
        return this;
      }
    }

    var mutable = asMutableArray.call(this);
    mutable[idx] = Immutable(value);
    return makeImmutableArray(mutable);
  }

  var immutableEmptyArray = Immutable([]);

  function arraySetIn(pth, value, config) {
    var head = pth[0];

    if (pth.length === 1) {
      return arraySet.call(this, head, value, config);
    } else {
      var tail = pth.slice(1);
      var thisHead = this[head];
      var newValue;

      if (typeof(thisHead) === "object" && thisHead !== null) {
        // Might (validly) be object or array
        newValue = Immutable.setIn(thisHead, tail, value);
      } else {
        var nextHead = tail[0];
        // If the next path part is a number, then we are setting into an array, else an object.
        if (nextHead !== '' && isFinite(nextHead)) {
          newValue = arraySetIn.call(immutableEmptyArray, tail, value);
        } else {
          newValue = objectSetIn.call(immutableEmptyObject, tail, value);
        }
      }

      if (head in this && thisHead === newValue) {
        return this;
      }

      var mutable = asMutableArray.call(this);
      mutable[head] = newValue;
      return makeImmutableArray(mutable);
    }
  }

  function makeImmutableArray(array) {
    // Don't change their implementations, but wrap these functions to make sure
    // they always return an immutable value.
    for (var index in nonMutatingArrayMethods) {
      if (nonMutatingArrayMethods.hasOwnProperty(index)) {
        var methodName = nonMutatingArrayMethods[index];
        makeMethodReturnImmutable(array, methodName);
      }
    }

    if (!globalConfig.use_static) {
      addPropertyTo(array, "flatMap",  flatMap);
      addPropertyTo(array, "asObject", asObject);
      addPropertyTo(array, "asMutable", asMutableArray);
      addPropertyTo(array, "set", arraySet);
      addPropertyTo(array, "setIn", arraySetIn);
      addPropertyTo(array, "update", update);
      addPropertyTo(array, "updateIn", updateIn);
    }

    for(var i = 0, length = array.length; i < length; i++) {
      array[i] = Immutable(array[i]);
    }

    return makeImmutable(array, mutatingArrayMethods);
  }

  function makeImmutableDate(date) {
    if (!globalConfig.use_static) {
      addPropertyTo(date, "asMutable", asMutableDate);
    }

    return makeImmutable(date, mutatingDateMethods);
  }

  function asMutableDate() {
    return new Date(this.getTime());
  }

  /**
   * Effectively performs a map() over the elements in the array, using the
   * provided iterator, except that whenever the iterator returns an array, that
   * array's elements are added to the final result instead of the array itself.
   *
   * @param {function} iterator - The iterator function that will be invoked on each element in the array. It will receive three arguments: the current value, the current index, and the current object.
   */
  function flatMap(iterator) {
    // Calling .flatMap() with no arguments is a no-op. Don't bother cloning.
    if (arguments.length === 0) {
      return this;
    }

    var result = [],
        length = this.length,
        index;

    for (index = 0; index < length; index++) {
      var iteratorResult = iterator(this[index], index, this);

      if (Array.isArray(iteratorResult)) {
        // Concatenate Array results into the return value we're building up.
        result.push.apply(result, iteratorResult);
      } else {
        // Handle non-Array results the same way map() does.
        result.push(iteratorResult);
      }
    }

    return makeImmutableArray(result);
  }

  /**
   * Returns an Immutable copy of the object without the given keys included.
   *
   * @param {array} keysToRemove - A list of strings representing the keys to exclude in the return value. Instead of providing a single array, this method can also be called by passing multiple strings as separate arguments.
   */
  function without(remove) {
    // Calling .without() with no arguments is a no-op. Don't bother cloning.
    if (typeof remove === "undefined" && arguments.length === 0) {
      return this;
    }

    if (typeof remove !== "function") {
      // If we weren't given an array, use the arguments list.
      var keysToRemoveArray = (Array.isArray(remove)) ?
         remove.slice() : Array.prototype.slice.call(arguments);

      // Convert numeric keys to strings since that's how they'll
      // come from the enumeration of the object.
      keysToRemoveArray.forEach(function(el, idx, arr) {
        if(typeof(el) === "number") {
          arr[idx] = el.toString();
        }
      });

      remove = function(val, key) {
        return keysToRemoveArray.indexOf(key) !== -1;
      };
    }

    var result = instantiateEmptyObject(this);

    for (var key in this) {
      if (this.hasOwnProperty(key) && remove(this[key], key) === false) {
        result[key] = this[key];
      }
    }

    return makeImmutableObject(result);
  }

  function asMutableArray(opts) {
    var result = [], i, length;

    if(opts && opts.deep) {
      for(i = 0, length = this.length; i < length; i++) {
        result.push(asDeepMutable(this[i]));
      }
    } else {
      for(i = 0, length = this.length; i < length; i++) {
        result.push(this[i]);
      }
    }

    return result;
  }

  /**
   * Effectively performs a [map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map) over the elements in the array, expecting that the iterator function
   * will return an array of two elements - the first representing a key, the other
   * a value. Then returns an Immutable Object constructed of those keys and values.
   *
   * @param {function} iterator - A function which should return an array of two elements - the first representing the desired key, the other the desired value.
   */
  function asObject(iterator) {
    // If no iterator was provided, assume the identity function
    // (suggesting this array is already a list of key/value pairs.)
    if (typeof iterator !== "function") {
      iterator = function(value) { return value; };
    }

    var result = {},
        length = this.length,
        index;

    for (index = 0; index < length; index++) {
      var pair  = iterator(this[index], index, this),
          key   = pair[0],
          value = pair[1];

      result[key] = value;
    }

    return makeImmutableObject(result);
  }

  function asDeepMutable(obj) {
    if (
      (!obj) ||
      (typeof obj !== 'object') ||
      (!Object.getOwnPropertyDescriptor(obj, immutabilityTag)) ||
      (obj instanceof Date)
    ) { return obj; }
    return Immutable.asMutable(obj, {deep: true});
  }

  function quickCopy(src, dest) {
    for (var key in src) {
      if (Object.getOwnPropertyDescriptor(src, key)) {
        dest[key] = src[key];
      }
    }

    return dest;
  }

  /**
   * Returns an Immutable Object containing the properties and values of both
   * this object and the provided object, prioritizing the provided object's
   * values whenever the same key is present in both objects.
   *
   * @param {object} other - The other object to merge. Multiple objects can be passed as an array. In such a case, the later an object appears in that list, the higher its priority.
   * @param {object} config - Optional config object that contains settings. Supported settings are: {deep: true} for deep merge and {merger: mergerFunc} where mergerFunc is a function
   *                          that takes a property from both objects. If anything is returned it overrides the normal merge behaviour.
   */
  function merge(other, config) {
    // Calling .merge() with no arguments is a no-op. Don't bother cloning.
    if (arguments.length === 0) {
      return this;
    }

    if (other === null || (typeof other !== "object")) {
      throw new TypeError("Immutable#merge can only be invoked with objects or arrays, not " + JSON.stringify(other));
    }

    var receivedArray = (Array.isArray(other)),
        deep          = config && config.deep,
        mode          = config && config.mode || 'merge',
        merger        = config && config.merger,
        result;

    // Use the given key to extract a value from the given object, then place
    // that value in the result object under the same key. If that resulted
    // in a change from this object's value at that key, set anyChanges = true.
    function addToResult(currentObj, otherObj, key) {
      var immutableValue = Immutable(otherObj[key]);
      var mergerResult = merger && merger(currentObj[key], immutableValue, config);
      var currentValue = currentObj[key];

      if ((result !== undefined) ||
        (mergerResult !== undefined) ||
        (!currentObj.hasOwnProperty(key)) ||
        !isEqual(immutableValue, currentValue)) {

        var newValue;

        if (mergerResult) {
          newValue = mergerResult;
        } else if (deep && isMergableObject(currentValue) && isMergableObject(immutableValue)) {
          newValue = Immutable.merge(currentValue, immutableValue, config);
        } else {
          newValue = immutableValue;
        }

        if (!isEqual(currentValue, newValue) || !currentObj.hasOwnProperty(key)) {
          if (result === undefined) {
            // Make a shallow clone of the current object.
            result = quickCopy(currentObj, instantiateEmptyObject(currentObj));
          }

          result[key] = newValue;
        }
      }
    }

    function clearDroppedKeys(currentObj, otherObj) {
      for (var key in currentObj) {
        if (!otherObj.hasOwnProperty(key)) {
          if (result === undefined) {
            // Make a shallow clone of the current object.
            result = quickCopy(currentObj, instantiateEmptyObject(currentObj));
          }
          delete result[key];
        }
      }
    }

    var key;

    // Achieve prioritization by overriding previous values that get in the way.
    if (!receivedArray) {
      // The most common use case: just merge one object into the existing one.
      for (key in other) {
        if (Object.getOwnPropertyDescriptor(other, key)) {
          addToResult(this, other, key);
        }
      }
      if (mode === 'replace') {
        clearDroppedKeys(this, other);
      }
    } else {
      // We also accept an Array
      for (var index = 0, length = other.length; index < length; index++) {
        var otherFromArray = other[index];

        for (key in otherFromArray) {
          if (otherFromArray.hasOwnProperty(key)) {
            addToResult(result !== undefined ? result : this, otherFromArray, key);
          }
        }
      }
    }

    if (result === undefined) {
      return this;
    } else {
      return makeImmutableObject(result);
    }
  }

  function objectReplace(value, config) {
    var deep          = config && config.deep;

    // Calling .replace() with no arguments is a no-op. Don't bother cloning.
    if (arguments.length === 0) {
      return this;
    }

    if (value === null || typeof value !== "object") {
      throw new TypeError("Immutable#replace can only be invoked with objects or arrays, not " + JSON.stringify(value));
    }

    return Immutable.merge(this, value, {deep: deep, mode: 'replace'});
  }

  var immutableEmptyObject = Immutable({});

  function objectSetIn(path, value, config) {
    if (!(path instanceof Array) || path.length === 0) {
      throw new TypeError("The first argument to Immutable#setIn must be an array containing at least one \"key\" string.");
    }

    var head = path[0];
    if (path.length === 1) {
      return objectSet.call(this, head, value, config);
    }

    var tail = path.slice(1);
    var newValue;
    var thisHead = this[head];

    if (this.hasOwnProperty(head) && typeof(thisHead) === "object" && thisHead !== null) {
      // Might (validly) be object or array
      newValue = Immutable.setIn(thisHead, tail, value);
    } else {
      newValue = objectSetIn.call(immutableEmptyObject, tail, value);
    }

    if (this.hasOwnProperty(head) && thisHead === newValue) {
      return this;
    }

    var mutable = quickCopy(this, instantiateEmptyObject(this));
    mutable[head] = newValue;
    return makeImmutableObject(mutable);
  }

  function objectSet(property, value, config) {
    var deep          = config && config.deep;

    if (this.hasOwnProperty(property)) {
      if (deep && this[property] !== value && isMergableObject(value) && isMergableObject(this[property])) {
        value = Immutable.merge(this[property], value, {deep: true, mode: 'replace'});
      }
      if (isEqual(this[property], value)) {
        return this;
      }
    }

    var mutable = quickCopy(this, instantiateEmptyObject(this));
    mutable[property] = Immutable(value);
    return makeImmutableObject(mutable);
  }

  function update(property, updater) {
    var restArgs = Array.prototype.slice.call(arguments, 2);
    var initialVal = this[property];
    return Immutable.set(this, property, updater.apply(initialVal, [initialVal].concat(restArgs)));
  }

  function getInPath(obj, path) {
    /*jshint eqnull:true */
    for (var i = 0, l = path.length; obj != null && i < l; i++) {
      obj = obj[path[i]];
    }

    return (i && i == l) ? obj : undefined;
  }

  function updateIn(path, updater) {
    var restArgs = Array.prototype.slice.call(arguments, 2);
    var initialVal = getInPath(this, path);

    return Immutable.setIn(this, path, updater.apply(initialVal, [initialVal].concat(restArgs)));
  }

  function asMutableObject(opts) {
    var result = instantiateEmptyObject(this), key;

    if(opts && opts.deep) {
      for (key in this) {
        if (this.hasOwnProperty(key)) {
          result[key] = asDeepMutable(this[key]);
        }
      }
    } else {
      for (key in this) {
        if (this.hasOwnProperty(key)) {
          result[key] = this[key];
        }
      }
    }

    return result;
  }

  // Creates plain object to be used for cloning
  function instantiatePlainObject() {
    return {};
  }

  // Finalizes an object with immutable methods, freezes it, and returns it.
  function makeImmutableObject(obj) {
    if (!globalConfig.use_static) {
      addPropertyTo(obj, "merge", merge);
      addPropertyTo(obj, "replace", objectReplace);
      addPropertyTo(obj, "without", without);
      addPropertyTo(obj, "asMutable", asMutableObject);
      addPropertyTo(obj, "set", objectSet);
      addPropertyTo(obj, "setIn", objectSetIn);
      addPropertyTo(obj, "update", update);
      addPropertyTo(obj, "updateIn", updateIn);
    }

    return makeImmutable(obj, mutatingObjectMethods);
  }

  // Returns true if object is a valid react element
  // https://github.com/facebook/react/blob/v15.0.1/src/isomorphic/classic/element/ReactElement.js#L326
  function isReactElement(obj) {
    return typeof obj === 'object' &&
           obj !== null &&
           (obj.$$typeof === REACT_ELEMENT_TYPE_FALLBACK || obj.$$typeof === REACT_ELEMENT_TYPE);
  }

  function isFileObject(obj) {
    return typeof File !== 'undefined' &&
           obj instanceof File;
  }

  function Immutable(obj, options, stackRemaining) {
    if (isImmutable(obj) || isReactElement(obj) || isFileObject(obj)) {
      return obj;
    } else if (Array.isArray(obj)) {
      return makeImmutableArray(obj.slice());
    } else if (obj instanceof Date) {
      return makeImmutableDate(new Date(obj.getTime()));
    } else {
      // Don't freeze the object we were given; make a clone and use that.
      var prototype = options && options.prototype;
      var instantiateEmptyObject =
        (!prototype || prototype === Object.prototype) ?
          instantiatePlainObject : (function() { return Object.create(prototype); });
      var clone = instantiateEmptyObject();

      if (true) {
        /*jshint eqnull:true */
        if (stackRemaining == null) {
          stackRemaining = 64;
        }
        if (stackRemaining <= 0) {
          throw new ImmutableError("Attempt to construct Immutable from a deeply nested object was detected." +
            " Have you tried to wrap an object with circular references (e.g. React element)?" +
            " See https://github.com/rtfeldman/seamless-immutable/wiki/Deeply-nested-object-was-detected for details.");
        }
        stackRemaining -= 1;
      }

      for (var key in obj) {
        if (Object.getOwnPropertyDescriptor(obj, key)) {
          clone[key] = Immutable(obj[key], undefined, stackRemaining);
        }
      }

      return makeImmutableObject(clone);
    }
  }

  // Wrapper to allow the use of object methods as static methods of Immutable.
  function toStatic(fn) {
    function staticWrapper() {
      var args = [].slice.call(arguments);
      var self = args.shift();
      return fn.apply(self, args);
    }

    return staticWrapper;
  }

  // Wrapper to allow the use of object methods as static methods of Immutable.
  // with the additional condition of choosing which function to call depending
  // if argument is an array or an object.
  function toStaticObjectOrArray(fnObject, fnArray) {
    function staticWrapper() {
      var args = [].slice.call(arguments);
      var self = args.shift();
      if (Array.isArray(self)) {
          return fnArray.apply(self, args);
      } else {
          return fnObject.apply(self, args);
      }
    }

    return staticWrapper;
  }

  // Wrapper to allow the use of object methods as static methods of Immutable.
  // with the additional condition of choosing which function to call depending
  // if argument is an array or an object or a date.
  function toStaticObjectOrDateOrArray(fnObject, fnArray, fnDate) {
    function staticWrapper() {
      var args = [].slice.call(arguments);
      var self = args.shift();
      if (Array.isArray(self)) {
          return fnArray.apply(self, args);
      } else if (self instanceof Date) {
          return fnDate.apply(self, args);
      } else {
          return fnObject.apply(self, args);
      }
    }

    return staticWrapper;
  }

  // Export the library
  Immutable.from           = Immutable;
  Immutable.isImmutable    = isImmutable;
  Immutable.ImmutableError = ImmutableError;
  Immutable.merge          = toStatic(merge);
  Immutable.replace        = toStatic(objectReplace);
  Immutable.without        = toStatic(without);
  Immutable.asMutable      = toStaticObjectOrDateOrArray(asMutableObject, asMutableArray, asMutableDate);
  Immutable.set            = toStaticObjectOrArray(objectSet, arraySet);
  Immutable.setIn          = toStaticObjectOrArray(objectSetIn, arraySetIn);
  Immutable.update         = toStatic(update);
  Immutable.updateIn       = toStatic(updateIn);
  Immutable.flatMap        = toStatic(flatMap);
  Immutable.asObject       = toStatic(asObject);
  if (!globalConfig.use_static) {
      Immutable.static = immutableInit({
          use_static: true
      });
  }

  Object.freeze(Immutable);

  return Immutable;
}

  var Immutable = immutableInit();
  /* istanbul ignore if */
  if (true) {
    !(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
      return Immutable;
    }.call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if (typeof module === "object") {
    module.exports = Immutable;
  } else if (typeof exports === "object") {
    exports.Immutable = Immutable;
  } else if (typeof window === "object") {
    window.Immutable = Immutable;
  } else if (typeof global === "object") {
    global.Immutable = Immutable;
  }
})();


/***/ })

/******/ });
});
//# sourceMappingURL=vanilla-storelocator.js.map