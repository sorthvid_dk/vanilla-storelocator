const webpack = require('webpack');
const WebpackDevServer = require("webpack-dev-server");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const cssUnit = require('postcss-units');

//const autoprefixer = require('autoprefixer');
//const ExtractTextPlugin = require('extract-text-webpack-plugin');
//const path = require('path');


var config = {
	context: __dirname + '/src',
	entry: {
		'vanilla-storelocator': ['./index.js'],
		'polyfill': ['babel-polyfill'],
		'maps': ['./scss/maps.scss']

	},
	output: {
		path: __dirname + '/dist',
		filename: '[name].js',
		publicPath: "/dist",
		libraryTarget: "umd"
	},
	module: {
		rules: [
			{
				test: /.js?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015']
				}
			},
			{
		        test: /\.scss$/,
		        use: ExtractTextPlugin.extract({
		        	fallback: "style-loader",
		        	use: [
		        		{ loader: "css-loader" },
		        		{ loader: "sass-loader" },
		        		{ loader: 'postcss-loader', options: {
		        			sourceMap: true,
		        			plugins: () => [autoprefixer, cssUnit({fallback:false, precision: 3})] }
		        		}
		        	]

	        	})
		    }

		]

	},
	devServer: {
  		open: true, // to open the local server in browser
  		contentBase: __dirname + '/.',
	},
	plugins: [
		new webpack.ProvidePlugin({
            'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
        }),
        new ExtractTextPlugin({
	    	filename: 'maps.css',
	       	disable: false,
	    	allChunks: true,
	    	disable: process.env.NODE_ENV === "development"
	    }),

	],
	devtool: "source-map"
};

if (process.env.NODE_ENV === "production") {
	config.devtool = "source-map";
	config.plugins.push(
    	new webpack.optimize.UglifyJsPlugin({
      		sourceMap: options.devtool && (options.devtool.indexOf("sourcemap") >= 0 || options.devtool.indexOf("source-map") >= 0),
      		compress: {
            	screw_ie8: true
            }
    	})
	)
}


module.exports = config;
