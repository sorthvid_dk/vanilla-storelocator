# Store Locator #
Store Locator is a Google Maps based store locator module.

## Documentation ##

## MicroTemplate concepts ##
Builds on the idea of ES6 literal template strings. A function named 'render' should always be in the class root which holds the template for the component.

## data models ##

###storeModel### 
data model for a store item.

###storeModel### 
Data model for a store.
```json
{
	address1: store name(String),
	address2: street name(String),
	zipCode: postal code (String),
	city: city(String),
	latitude: Latitude(Number),
	longitude: Longitude(Number),
	id: unique id(Number),
	sid: unique store id (Number),
	countryCode: ISO countrycode(String),
	storeType: points to a storeTypeModel id(number)

}
```
###storeAttributeModel### 
Data model for store attributes, used as filter items and icons on makers.
```json
{
	sid: unique id(String/Number),
	name: name of the store type(String),
	markerIcon: 'src/{.png,.svg}',
	filterIcon: 'src/{.png,.svg}',
	weight: filter weight (Number)
	isFlagshipStore: wins over weight(Bool) 
}
```

###detailModel###
data model for details view 
```json
{
	address1: store name(String),
	address2: street name(String),
	zipCode: zipcode(String),
	city: city(String),
	distance: null(Number) - leave it empty is calculated in frontend,
	openingHours: [ // array of weekdays
		{
			label: weekday line title(String) ex. Monday,
			hours: opening hours(String) ex. 09.00-16.00
		}
	],			
	links: [ //array of links, shown in info overlay
		{
		label: Label before link(String),
		text: link text(String),
		href: link href attribute(String)
	]
}
```


### events ###
Events can be set in an Object with the format ´´´{ [eventType] [selector]: [eventHandler] }´´´

transitionend event will fire the eventHandler 'onTransitionEnd' if it's defined. And a transitionend event bubbles to main element. 

## Browser support ##
Internet Explorer 10+
Chrome latest
Safari latest
Firefox latest
Opera latest

## Changelog ##
* 0.0.1: first version 

## Defaults ##

* uri: {String} google map API url (//maps.googleapis.com/maps/api/js)
* libraries: {Array} - array of google maps libraries to include
* callbackFn: {String} - callback function to register globaly
* zoomLevelOnListClick: {Num} - zooom level on click on list
* countryFilterOnInitialize: {String} - ISO county code ex. ("DK"),
* countryFilterOnInitializeKey {String}: - key to filter parsed data 
* markerClusterer {Bool} - add clusterer script. (default: true)
* markerClustererStart {Bool} - add clusterer at provided zoomLevel (default: 8)



## Releases ##

* v1.0.1: 

## Changelog ##
* v1.0.0: bug fixes: #8, #9 and new version branches for laurie and beoplay
* v0.0.1: first version 