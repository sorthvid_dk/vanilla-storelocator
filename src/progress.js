import { defaults } from './index';
import { formatSelector as _css} from './utils';
import MicroTemplate from './microtemplate';
class Progress extends MicroTemplate {
	constructor(data){
		super(data);
		this.body = document.body;
		this.delegateEvents({
			'transitionend': 'onTransitionEndRemove',
			'webkitTransitionEnd': 'onTransitionEndRemove',
			'click': 'onTransitionEndRemove'
		});
	}

	onTransitionEndRemove() {
		this.el.classList.remove('fade-out');
		super.destroy();
	}

	start(data) {
		if(data) {
			this.reRender(data);
		}
		this.body.appendChild(this.el);
	}

	stop() {
		this.el.classList.add('fade-out');
	}
	/*jshint quotmark: double */
	render(data) {
		var message = data.message || '';
		return `<div class="${_css(defaults.cssClass.progress)}">
			<div class="${_css(defaults.cssClass.progressMsg)}">
				<img src="${defaults.spinnerUrl}" alt="image of a waiting graphic" />
				<p>${message}</p>

			</div>
		</div>`;
	}
}

export default Progress;
