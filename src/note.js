import { defaults } from './index';
import MicroTemplate from './microtemplate';

import { formatSelector as _css} from './utils';

class Note extends MicroTemplate { //storeAttributeModel
	constructor(data) {
		super(data);
		var _listners = {
			'click .button': 'onClick'
		};
		this.delegateEvents(_listners);
		this.isSelected = false;
	}

	onClick(e){
		this.data.onClick(e);
	}

	render(data){
		var subtitle = data.subTitle ? `<span class="${_css(defaults.cssClass.noteSubTitle)}">${data.subTitle}</span>` : '';
		return `<li class="${_css(defaults.cssClass.note)}">
			<h2>${data.title}</h2>
			${subtitle}
			<a href="#" class="button button-primary">${data.button.text}</a>
		</li>`;
	}

}


export default Note;
