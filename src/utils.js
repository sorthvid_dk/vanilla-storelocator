
// Tagged template function
function formatSelector(selector) {
    if(!selector) return;
    return selector.replace(/[.,\/#\[\]!$%\^&\*;:{}=\\]/g, '');
}
function find(arg0, arg1) {
	var result;
	if ( typeof arg1 == 'undefined' ) {
		result = document.querySelectorAll(arg0);
	}
	else {
		result = arg0.querySelectorAll(arg1);
	}
	return result;
}
function each(nodeList, cb) {
	for(var i=0; i<nodeList.length; i++) {
		cb(nodeList[i], i);
	}
}
function queryParam(obj) {
	let esc = encodeURIComponent;
	let query = Object.keys(obj).map(k => {
		return esc(k) + '=' + esc(obj[k]);
	});
	return query.join('&');
}
function featureTest(feat) {
	var b = document.body || document.documentElement,
        s = b.style,
        p = feat;

    if (typeof s[p] == 'string') { return true; }

    // Tests for vendor specific prop
    var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
    p = p.charAt(0).toUpperCase() + p.substr(1);

    for (var i=0; i<v.length; i++) {
        if (typeof s[v[i] + p] == 'string') { return true; }
    }
    return false;
}

export { formatSelector, find, each, queryParam, featureTest};
