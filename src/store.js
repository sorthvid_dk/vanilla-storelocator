import { defaults } from './index';
import { ev } from './settings';
import { formatSelector as _formatSelector} from './utils';
import MicroTemplate from './microtemplate';
import Events from './events';





class Store extends MicroTemplate {
	constructor(data, map){
		super(data);
		this.isSelected = Events.state.selectedStore === this.data.id;
		this.isVisible = true;
		this.map = map;

		this.storeType = defaults.storeAttributes.filter((storeAttr)=>{
			return storeAttr.sid.toString() === this.data.sid.toString();
		});
		this.markerIcons = {
			default: new google.maps.MarkerImage(this.storeType[0].markerIcon.url, null, null, null, new google.maps.Size(this.storeType[0].markerIcon.w, this.storeType[0].markerIcon.h)),
			selected: new google.maps.MarkerImage(this.storeType[0].markerIconSelected.url, null, null, null, new google.maps.Size(this.storeType[0].markerIconSelected.w, this.storeType[0].markerIconSelected.h)),
		    hover: this.storeType[0].markerIconHover ? new google.maps.MarkerImage(this.storeType[0].markerIconHover.url, null, null, null, new google.maps.Size(this.storeType[0].markerIconHover.w, this.storeType[0].markerIconHover.h)) : null,
		};
		this.eventCache = [];
		this.setMarker(this.map);

		// html events
		var _listners = {
			'click': 'onListItemClick',
			'mouseover': 'onMouseOver',
			'mouseout': 'onMouseOut'
		};
		_listners['click ' + defaults.html.infoItem] = 'onInfoClick';
		this.delegateEvents(_listners);

		//App events

		this._onMarkerClick = this.onMarkerClick.bind(this);
		this._setSelection = this.setSelection.bind(this);
		this._destroy = this.destroy.bind(this);

		Events.on(ev.markerClick, this._onMarkerClick);
		Events.on(ev.selectedStore, this._setSelection);
		Events.on(ev.clearList, this._destroy);

		// map is draged
		this.eventCache.push( this.map.addListener('dragend', this.onMapChange.bind(this)) );
		// zoom is changed
		this.eventCache.push( this.map.addListener('zoom_changed', this.onMapChange.bind(this)) );
		this.updateView({setCenter: false});

	}

	/**
	* setMarkers is called on instantiation
	*
	*/
	setMarker(map) {
		var pos = new google.maps.LatLng(this.data.latitude, this.data.longitude);
		this.marker = new google.maps.Marker({
			position: pos,
			map: map,
			icon: this.markerIcons.default,
			clickable: true,
			id: this.data.id,
			optimized: false
		});
		this.eventCache.push(google.maps.event.addListener(this.marker, 'click', ()=>{
			Events.emit(ev.markerClick, this);
		}));
		this.eventCache.push(google.maps.event.addListener(this.marker, 'mouseover', this.onMouseOver.bind(this)));
		this.eventCache.push(google.maps.event.addListener(this.marker, 'mouseout', this.onMouseOut.bind(this)));
	}

	/**
	* onMarkerClick
	*
	*/
	onMarkerClick(e) {
		this.isSelected = this.data.id === e.data.id;
		this.updateView({setCenter: true});
		if(this.isSelected){
			this.onInfoClick(e);
			Events.setState(ev.selectedStore, ['selectedStore'], this.data.id);
		}
	}

	onListItemClick(e) {
		e.preventDefault();
		Events.setState(ev.selectedStore, ['selectedStore'], this.data.id);
	}

	/** TODO NOT USED AT THE MOMENT
	* calculate the shop visibility by the default setting 'storeRadius'. if a Place search is performed it will overrule the storeRadius setting.
	*
	*/
	onMapChange() {
		//var onMap = this.map.getBounds().contains(this.marker.getPosition());
		//console.log('STORE - onmapchange');
		//this.toggleState(onMap)
	}

	/**
	* afterRender - runs after component template is recompiled
	* @private
	*/
	afterReRender() {
		this.isSelected = Events.state.selectedStore === this.data.id;
		this.updateView({setCenter: false});
	}

	/**
	* onInfoClick
	* @private
	*/
	onInfoClick(e) {
		if(defaults.html.details) {
			if(e.stopPropagation) e.stopPropagation();
			Events.setState(ev.showInfo, ['openInfoOverlay'], this.data.id);
		}
	}

	/**
	*  on_SetSelection
	*/
	setSelection(state) {
		if(!this.marker) {
			console.warn('event error looks like you have a some garbage listners');
			return;
		}
		this.isSelected = this.data.id === state.selectedStore;
		this.updateView({setCenter: true});
		this.map.setZoom(defaults.onSetSelectionZoom);
	}


	/*
	* updates view
	* @private
	*/
	updateView({setCenter=true}) {
		//highlight list item
		this.el.classList.toggle(defaults.cssClass.isActive, this.isSelected);
		//change marker
		if(this.isSelected) {
			this.marker.setIcon(this.markerIcons.selected);
			if(setCenter) {
				this.map.setCenter(new google.maps.LatLng(this.marker.position.lat(), this.marker.position.lng()));
			}
		} else {
			this.marker.setIcon(this.markerIcons.default);
		}
	}

	/**
	* onMouseOver
	* @private
	*/
	onMouseOver() {
		this.el.classList.add(defaults.cssClass.isHover);
		if(!this.isSelected) {
			var icon = this.markerIcons.hover ? this.markerIcons.hover : this.markerIcons.selected;
			this.marker.setIcon(icon);
		}

	}

	/**
	* onMouseOut
	* @private
	*/
	onMouseOut() {
		this.el.classList.remove(defaults.cssClass.isHover);
		if(!this.isSelected) {
			this.marker.setIcon(this.markerIcons.default);
		}
	}

	/**
	* setActive - public function to set stores as selected
	* @public
	*/
	setSelected() {
		Events.setState(ev.selectedStore, ['selectedStore'], this.data.id);
		//Events.setState()
	}
	/**
	* destroy
	* @public
	*/
	destroy() {
		this.eventCache.forEach(item=>{
			google.maps.event.removeListener(item);
		});

		Events.off(ev.markerClick, this._onMarkerClick);
		Events.off(ev.selectedStore, this._setSelection);
		Events.off(ev.clearList, this._destroy);

		this.marker.setMap(null);
		this.marker=null;
		super.destroy();
	}

	/**
	* render
	* @private
	*/
	/*jshint quotmark: double */
	render(data) {
		var distance = () => {
			if(data.distanceToGeoLocation) {
				return `<span>${data.distanceToGeoLocation + ' ' + defaults.labels.distanceUnit + ' ' + defaults.labels.distance } </span>`;
			} else {
				return '';
			}
		};
		var icon = () => {
			var storeIcons = defaults.storeAttributes.filter((storeAttr)=>{
				return storeAttr.sid === this.data.sid;
			});
			if(storeIcons.length) {
				return `<img src="${storeIcons[0].filterIcon}" alt="Store icon">`;
			} else {
				return '';
			}
		};
		return `<li role="button" ${_formatSelector(defaults.html.listItem)}>
			<article class="${_formatSelector(defaults.cssClass.storeItem)}">
				${ icon() }
					<div class="${_formatSelector(defaults.cssClass.storeItemInner)}">
					<h3 class="title">${data.address1}</h3>
					<p>${data.address2}</p>
					<p>${data.zipCode} ${data.city}</p>
					<p>
						${ distance() }
						<a href="#" ${_formatSelector(defaults.html.infoItem)}>${defaults.labels.moreInfo}</a>
					</p>
				</div>
			</article>
		</li>`;
	}
}

export default Store;
