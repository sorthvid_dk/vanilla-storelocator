//https://www.npmjs.com/package/es6-event-emitter
import EventEmitter from 'event-emitter-es6';
import Immutable from 'seamless-immutable';

class Events extends EventEmitter {
	constructor(){
		super();
		this.hasEventListeners = !!window.addEventListener;
		this.state = {};

	}
	addEvent(el, e, callback, capture) {
	    var els = el instanceof Element ? [el] : el;
	    [].forEach.call(els, el=>{
	    	if (this.hasEventListeners) {
	        	el.addEventListener(e, callback, !!capture);
	        } else {
	            el.attachEvent('on' + e, callback);
	        }
	    });
	}

	removeEvent(el, e, callback, capture) {
	    var els = el instanceof Element ? [el] : el;
	    	[].forEach.call(els, el=>{
		    if (this.hasEventListeners) {
		        el.removeEventListener(e, callback, !!capture);
		    } else {
		        el.detachEvent('on' + e, callback);
		    }
		});
	}

	trigger(el, eventName, detail) {
	    var e;
		if(window.CustomEvent) {
			e = new CustomEvent(eventName, { bubbles: true, cancelable: true, detail: detail });
		} else {
			e = document.createEvent('Event');
			e.initEvent(eventName, true, true, { detail: detail });
			e.detail = detail;
		}
		el.dispatchEvent(event);
	}
	initState(state){
		this.state = Immutable.from(state);
		this.stateActivated = true;
	}

	/**
	* mergeState is an immutable helper that merge a state object. returns this new state.
	* @param eventName {String} - a static eventName to emit immitialty after the state is updated
	* @param key {Array} - keys to select
	* @param newVal {all} - new value to set in Immutable state object
	* @param deep {Bool} - performs a deep compare (default to false)
	*/
	mergeState(eventName, newState, deep){
		this.state = Immutable.merge(this.state, newState, {deep: deep});
		this.emit('state:changed', this.state);
		this.emit(eventName, this.state);
		return this.state;
	}

	/**
	* setState is an immutable helper that set a state in a particular place of the object. returns this new state.
	* @param eventName {String} - a static eventName to emit immitialty after the state is updated
	* @param key {Array} - keys to select
	* @param newVal {all} - new value to set in Immutable state object
	* @param deep {Bool} - performs a deep compare (default to false)
	*/

	setState(eventName, key, newVal, deep){
		deep = deep || false;
		this.state = Immutable.setIn(this.state, key, newVal, {deep: deep});
		this.emit('state:changed', this.state);
		this.emit(eventName, this.state);
		return this.state;
	}
	getState(query, defaultVal){
		defaultVal = defaultVal || null;
		return Immutable.getIn(this.state, query);
	}

}
export default new Events();

