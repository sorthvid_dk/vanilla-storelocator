import Events from './events';
import { find as _find, each as _each, } from './utils';
import Immutable from 'seamless-immutable';

class MicroTemplate {
	constructor(data){
		this.data = data;
		this.eventListeners = [];
		var root = document.createDocumentFragment(),
			el = document.createElement('span');
		root.appendChild(el);
		this.rootEl = root.childNodes[0]; //span: used as container for rendering template
		this.preRender(data);
	}
	delegateEvents(events) {
		if ( this.eventListeners.length > 0 ) throw new Error('Event listeners have already been delegated!');
		events = Immutable.merge({
			'transitionend': 'onTransitionEnd',
		}, events);
		this.events = events;

		for (let prop in events) {
			let eventSplit = prop.split(' ');
			let eventName = eventSplit[0];

			// is the target specified with a selector, or is this.el implied?
			let target = eventSplit.length > 1 ? eventSplit[1] : this.el;
			if(!this[events[prop]]) { throw new Error('MicroTemplate is missing valid eventhandler', this); }
			let eventHandler = this[events[prop]].bind(this);

			// is the target already an element or a selector?
			let elements;
			if ( typeof target == 'string' ){
				elements = _find(this.el, target);
			}
			else if ( target.length && target != this.el ) {
				elements = _find(this.el, target[0]);
			}
			else {
				elements = [this.el];
			}
			_each(elements, (element)=>{ /*jshint ignore:line */
				this.eventListeners.push({element: element, eventName: eventName, eventHandler: eventHandler});
				Events.addEvent(element, eventName, eventHandler, false);
			});
		}
	}
	undelegateEvents() {
		this.eventListeners.forEach((listener)=>{
			Events.removeEvent(listener.element, listener.eventName, listener.eventHandler);
		});
		this.eventListeners = [];
		return true;
	}
	onTransitionEnd() {
		return;
	}

	//TODO: check validity of html before letting the browser parse it.
	/*
	*  @private
	*/
	preRender(data){
		data = data || {};
		let el = this.render(data);
		if(!el) el = '<span></span>';
		if(!this.isValidHtml(el)) console.warn('Mirotemplate - Somethings wrong with the markup, typically a missing quote..');
		this.rootEl.innerHTML = el;
		this.el = this.rootEl.childNodes[0];
		this.afterRender();
	}
	/*
	* @private
	*/
	isValidHtml(str) {
	    //if((str.match(/\"/g) || []).length%2 != 0) return false;
	    var doc = new DOMParser().parseFromString(str, 'text/html');
    	return Array.from(doc.body.childNodes).some(node => {
    		if(node.nodeType != 1) console.warn('Microtemplate - try to find this node', node);
    		return node.nodeType === 1;
    	});
	}
	/*
	*  @private
	*/
	destroyElement() {
		if(this.undelegateEvents()) {
			this.el.classList.add('off');
			if(this.el.remove) {
				this.el.remove();
			} else {
				this.el.parentNode.removeChild(this.el);
			}
		}
	}

	/*
	* @public
	*/
	html() {
		this.isRendered = true;
		return this.el;
	}

	/*
	* @public
	*/
	reRender(data) {
		this.destroyElement();
		this.preRender(data);
		this.delegateEvents(this.events);
		this.afterReRender();
	}
	/*
	* @public
	*/
	afterRender() {
		return;
	}
	/*
	* @public
	*/
	afterReRender() {
		return;
	}
	/*
	* @public
	*/
	render(){
		console.warn('no markup defined for microcomponent');
	}
	/*
	*  @public
	*/
	destroy(){
		this.destroyElement();

	}

}
export default MicroTemplate;
