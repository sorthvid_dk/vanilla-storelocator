import mapStyles from './map-config';
import {each as _each, queryParam as _queryParam, featureTest as _feat} from './utils';
import {includes as _includes } from 'lodash';
import Immutable from 'seamless-immutable';
import Events from './events';
import Store from './store';
import Note from './note';
import StoreAttribute from './store-attribute';

import { ev, defaults as _settings } from './settings';
import Progress from './progress';
// import Notification from './notification';
import InfoOverlay from './infooverlay';

//let defaults = _merge({}, _settings);
let defaults = Immutable.merge({}, _settings, {deep: true});

// this.rootEl = Emmet(el);
// this.el = this.rootEl.childNodes[0];

class StoreLocator {
	constructor(props) {
		//TODO: test if google api is pressent
		//TODO: deep assign
		defaults = Immutable.merge(defaults, props, {deep: true});

		if(defaults.html.loaderElement){
			var loaderElement = document.querySelector(defaults.html.loaderElement);
			loaderElement.classList.add('shown');
		} else {
			this.Progress = new Progress({message: defaults.labels.introText});
			this.Progress.start();
		}

		//init state
		Events.initState({
			selectedStore: null,
			activeFilters: [],
			activePanel: 'main',
			data: {},
  			zoomLevel: defaults.mapOptions.zoom,
  			notifications: [],
  			location: null,
  			geoLocation: false,
  			filteredData: [],
  			support: {
  				transition: _feat('transition')
  			}
		});

		this._timeout = window.setTimeout;

		//check for smallscreen TODO build it more solid
		this.isSmallScreen = window.matchMedia ? window.matchMedia('(max-width: 750px)').matches : false;


		//html elements
		this.html = defaults.html.container instanceof Element ? defaults.html.container : document.querySelectorAll(defaults.html.container)[0];
		if(!this.html) { console.warn('Storelocator is missing a container'); return; }
		this.listContainer = this.html.querySelectorAll(defaults.html.list)[0];
		this.list = this.listContainer.getElementsByTagName('ul')[0];
		this.input = this.html.querySelectorAll(defaults.html.input)[0];

		this.mainPanel = this.html.querySelectorAll(defaults.html.mainPanel)[0];

		//infooverlay
		this.infoOverlay = new InfoOverlay();

		//filter container
		this.filterAttributes = this.html.querySelectorAll(defaults.html.storeAttribute)[0];

		if(this.filterAttributes){
			defaults.storeAttributes.forEach((attr)=>{
				this.filterAttributes.appendChild(new StoreAttribute(attr).el);
			});
		}

		//injects map script in document
		this.injectMapsApi();

		// HTML Events

		Events.addEvent(document, 'keydown', this.onKeyDownGlobal.bind(this), false);
		Events.addEvent(document, 'click', this.onClickGlobal.bind(this), false);
		Events.addEvent(this.html.querySelectorAll(defaults.html.geoLocation), 'click', this.onGeoLocationClick.bind(this), false);
		Events.addEvent(this.html.querySelectorAll(defaults.html.searchButton), 'click', this.onSearchClick.bind(this), false);
		Events.addEvent(this.html.querySelectorAll(defaults.html.panelController), 'click', this.onPanelControlClick.bind(this), false);

		// App events
		Events.on(ev.selectedStore, this.onSelectedStore.bind(this));
		Events.on(ev.showInfo, this.onShowInfo.bind(this));
		Events.on(ev.hideInfo, ()=>{
			this.listContainer.classList.remove('no-overflow');
		});

		Events.on(ev.setFilter, this.filterChange.bind(this));
		// Events.on('state:changed', (state)=>{ //loggin all state changes
		// 	console.log('state changed:', state);
		// });


	}

	/*
	* inject maps api scriptblock
	* @Private
	*/
	injectMapsApi() {
		var mapsScript = document.createElement('script');
		var urlString = [];
		urlString.push(window.location.protocol);
		urlString.push(defaults.uri + '?');
		urlString.push('libraries=' + defaults.libraries.join(',') + '&');
		urlString.push('key=' + defaults.apiKey + '&');
		urlString.push('callback=' + defaults.callbackFn);
		if(defaults.languageCode){
			urlString.push('&language=' + defaults.languageCode);
		}
		if(defaults.countryCode){
			urlString.push('&region=' + defaults.countryCode);
		}
		mapsScript.setAttribute('src', urlString.join(''));
		mapsScript.setAttribute('async', '');
		document.head.appendChild(mapsScript);
		window[defaults.callbackFn] = this.initMap.bind(this);
		if(defaults.markerClusterer) {
			var clusterScript = document.createElement('script');
			clusterScript.setAttribute('src', '//developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js');
			clusterScript.setAttribute('async', '');
			document.head.appendChild(clusterScript);
		}
	}


	/******** Public functions *******/

	/*
	*  bounded to window object and called when map API is ready.
	*  @private
	*/
	initMap() {
		const clientMapTypeId = 'client_style';
		var mapContainer = this.isSmallScreen && defaults.html.mapMobile ? this.html.querySelectorAll(defaults.html.mapMobile)[0] : this.html.querySelectorAll(defaults.html.map)[0];
		if(!mapContainer) {
			console.warn('Missing map container: ', mapContainer);
			return;
		}
		this.clientMapType = new google.maps.StyledMapType(mapStyles, { name: 'client_style' });


		this.map = new google.maps.Map(mapContainer, defaults.mapOptions);
		this.map.mapTypes.set(clientMapTypeId, this.clientMapType);

		google.maps.event.addListenerOnce(this.map, 'tilesloaded', ()=>{
			// Global ready event
			Events.setState(ev.mapReady, ['isSmallScreen'], this.isSmallScreen);
			// Load storedata
			this.getShops();
			// Fake loading delay
			setTimeout(()=>{
				if(defaults.html.loaderElement){
					var loaderElement = document.querySelector(defaults.html.loaderElement);
					loaderElement.classList.remove('shown');
				} else {
					this.Progress.stop();
				}
				this.html.classList.add(defaults.cssClass.ready);
			}, defaults.fakeLoadingDelay);
		});

		this.map.addListener('drag', this.onMapDrag.bind(this));
		this.map.addListener('idle', this.onMapIdle.bind(this));
		this.map.addListener('zoom_changed', this.onMapChange.bind(this));

		// Autocomplete
		this.autocomplete =  new google.maps.places.Autocomplete(this.input);
		this.autocomplete.addListener('place_changed', this.onPlaceChange.bind(this));

	}

	/**
	*  Handles an autocomplete request for new place search
	*  @Private
	*/
	onPlaceChange(place) {
		place = place || this.autocomplete.getPlace();
		if(Events.state.location && this.locationMarker) {
			this.locationMarker.setMap(null);
			this.locationMarker = null;
		}
		if(place.geometry) {
			var icon = new google.maps.MarkerImage(defaults.icons.places.url, null, null, null, new google.maps.Size(defaults.icons.places.w, defaults.icons.places.h));
			this.locationMarker = this.marker = new google.maps.Marker({
				position: place.geometry.location,
				map: this.map,
				icon: icon,
				clickable: true,
				draggable: true,
				animation: google.maps.Animation.DROP,
				title: defaults.labels.yourSearchMarkerLabel
			});
			this.locationMarker.setAnimation(google.maps.Animation.BOUNCE);
			window.setTimeout(()=>{
				this.locationMarker.setAnimation(null);
			}, defaults.bounceTimeout);

			Events.setState(ev.placeChange, ['location'], place.formatted_address); // jshint ignore:line

			if (place.geometry.viewport) {
        		this.map.fitBounds(place.geometry.viewport);
        	} else {
       			this.map.setCenter(place.geometry.location);
        		this.map.setZoom(17);  // Why 17? Because it looks good.
        	}
		}
	}


	/**
	*  Intialize shop list. This function intializes all store intances from first fetch. should only be called once
	*  @public
	*  @param {Array} data - Array with stores to be added to the map. The struckture is documented in readme.md
	*/
	setStoreData(data){
		// data is comming from an external service executed outside of StoreLocator class.
		var filteredData = [];
		this.stores = [];
		Events.setState(ev.setStoreData, ['data'], data, true);

		if(defaults.mapData.initFilter) {
			filteredData = data.filter(defaults.mapData.initFilter);
		} else {
			filteredData = data;
		}
		filteredData = this.setDistanceToCenter(filteredData);
		this.createStores(filteredData);
		this.setListSize();


	}

	/**
	*  createStores - main update step in the application it creates stores and reRender stores based on the filterdData provided
	* @param { Array } - array of data to publish to UI
	*/
	createStores(filteredData) {
		//var stores = Events.get(['stores']);
		//destroy existing stores

		var cachedStores = [];
		this.stores.forEach(store=>{
			if(this.isStoreInBound(store.data) ) { //already exist or under a filter  removed(|| selectedStoreId === store.data.id)
				cachedStores.push(store);
			} else {
				store.destroy();
			}
		});
		//filter
		if(Events.state.activeFilters.length) {
			cachedStores.forEach((store)=>{
				if(!this.filterStoreAttribute(store.data, 'sid')) {
				 	store.destroy();
				}
			});
		}

		this.stores = filteredData.map(storeModel=>{ //create new stores and makers
			var thisIsCachedStore = cachedStores.find(item=>{
				return (item.data.id === storeModel.id && item.marker);
			});
			if(cachedStores.length && thisIsCachedStore) { // performance
				thisIsCachedStore.reRender(storeModel);
				this.list.appendChild(thisIsCachedStore.html());
				return thisIsCachedStore;
			} else {
				var item = new Store(storeModel, this.map);
				this.list.appendChild(item.html());
				return item;
			}
		});

		Events.setState('app:updatefiltereddata', ['filteredData'], filteredData);
		this.markers = this.getMarkers();

		// clusters
		if(defaults.markerClusterer) {
			if(this.markerCluster){
				this.markerCluster.clearMarkers();
			}
			this.markerCluster = new window.MarkerClusterer(this.map, this.markers, {
			 	imagePath: defaults.icons.cluster.url,
			 	setMaxZoom: defaults.maxZoomLevelClusterer,
				gridSize: defaults.clustererGridSize || 40
			});
		}
	}

	/****** Private methods ********/

	/**
	*  onSetSelected
	*/
	onSelectedStore(state) {
		this.selectedStore = this.stores.find(store=>{
			return store.data.id === state.selectedStore;
		});
		this.getStoresInBound();
	}

	/*
	* get markers available
	* @private
	*
	*/
	getMarkers() {
		return this.stores.map(store=>{
			return store.marker;
		});
	}


	/****** Private event handlers ********/

	/**
	*  set number of stores text in UI
	*/
	setListSize() {
		if(defaults.html.listSize){
			this.html.querySelectorAll(defaults.html.listSize)[0].textContent = this.stores.length;
		}
	}

	/*
	* onMapChange - zoom and pan eventHandler
	*
	*/
	onMapChange() {

		var zoom = this.map.getZoom();
		if(zoom > defaults.zoomLevelFilterStoresInBound) {
			this.getStoresInBound();
		}
		Events.setState(ev.zoom, ['zoom'], zoom);
	}

	/**
	*  onMapDrag - proxy function that ensures that panning is possible
	*/
	onMapDrag() {
		Events.setState(ev.drag, ['draggin'], true);
	}

	/**
	*  onMapIdle - event listner for maps idle event
	*/
	onMapIdle() {
		if(this.map.getZoom() > defaults.zoomLevelFilterStoresInBound && Events.state.draggin) {
			this.getStoresInBound();
			Events.setState(ev.drag, ['draggin'], false);
		}

	}

	/**
	*  onShowInfo
	*/
	onShowInfo(state) {
		this.getStoreDetails(state.openInfoOverlay).then(detailModel=>{
			this.infoOverlay.reRender(detailModel);
			if(this.isSmallScreen) {
				this.html.appendChild(this.infoOverlay.html());
			} else {
				this.html.querySelectorAll(defaults.html.sidebar)[0].appendChild(this.infoOverlay.html());
			}
			this.infoOverlay.setActive();
			window.setTimeout(()=>{
				this.infoOverlay.el.classList.add(defaults.transitionIn);
			}, 50);

		});


	}

	/**
	*  setDistanceToCenter
	*/
	setDistanceToCenter(collection) {
		var center = this.map.getCenter();
		collection = [].concat(collection);
		collection = collection.map(item=>{
			var markerPos = new google.maps.LatLng(item.latitude, item.longitude);
			item = Immutable.asMutable(item);
			item.distanceToCenter = google.maps.geometry.spherical.computeDistanceBetween(center, markerPos);
			return item;
		});
		collection.sort((a, b)=>{
			return parseFloat(a.distanceToCenter) - parseFloat(b.distanceToCenter);
		});
		return collection;
	}

	/**
	*  setDistanceToGeoLocation
	*/
	setDistanceToGeoLocation(collection) {
		if(!Events.state.geoLocation) return false;

		collection = collection.map(item=>{
			var markerPos = new google.maps.LatLng(item.latitude, item.longitude);
			item = Immutable.asMutable(item);
			item.distanceToGeoLocation = Math.round(google.maps.geometry.spherical.computeDistanceBetween(Events.state.geoLocation, markerPos)/1000);
			return item;
		});
		return collection;


	}

	/**
	*  getStoreDetails
	*/
	getStoreDetails(id) {
		if(defaults.services.details) {
			var url = defaults.services.details + '?' + _queryParam({'id': id});
			return fetch(url, {
				method: 'GET'
			})
			.then(res => res.json());

		} else {
			let data = Events.state.filteredData;
			return new Promise((resolve, reject)=>{
				let shop = data.filter(item=>{
					return item.id === id;
				});
				resolve(shop[0]);
				reject('Error: no local data found');
			});
		}
	}

	/**
	*  getStoresInBound
	*/
	getStoresInBound() {
		var filteredData;
		var data = this.getData();
		// get stores currently active on the map
		filteredData = data.filter(storeModel=>{
			return this.isStoreInBound(storeModel);
		});
		// filter from filters
		if(Events.state.activeFilters.length) {
			filteredData = filteredData.filter(storeModel=>{
				return this.filterStoreAttribute(storeModel, 'sid');
			});
		}
		if(!filteredData.length) { // there are no stores in the filtered data find the nearst store
			this.noStoresInBound();
			//Events.emit(ev.noStoresInBound);
		} else {
			if(this.note) this.note.destroy();
			filteredData = this.setDistanceToCenter(filteredData);
			filteredData = Events.state.geoLocation ? this.setDistanceToGeoLocation(filteredData) : filteredData;
		}
		this.createStores(filteredData);
		this.setListSize();
	}

	/**
	*  filterStore
	*/
	filterStoreAttribute(storeModel, key) {
		return _includes(Events.state.activeFilters, storeModel[key]);
	}

	/**
	*  isStoreInBound
	*/
	isStoreInBound(storeModel) {
		var mapBounds = this.map.getBounds();
		return mapBounds.contains({lat: storeModel.latitude, lng: storeModel.longitude});
	}

	/**
	* get data from data service. TODO: need to be
	* @public
	*/
	getData() {
		return Events.state.data;
	}

	getShops() {
		fetch(defaults.services.shops)
		.then(res => res.json())
		.then(data => {
			//var filteredData = data.filter(defaults.mapData.initFilter)
			data = defaults.apiDataKey ? data[defaults.apiDataKey] : data;
		    var transformedData = data.map( defaults.mapData.storeModel );
		    this.setStoreData( transformedData );
		});
	}

	/**
	*  getLocationFromAddress
	*/
	getLocationFromAddress(address) {
		var geocoder = new google.maps.Geocoder();
		return new Promise((resolve, reject)=>{
			geocoder.geocode( { 'address': address}, (results, status) => {
				if (status == google.maps.GeocoderStatus.OK) {
					resolve(results);
				} else {
				  reject('Geocode was not successful: ' + status);
				}
			});
		});
	}

	/**
	*  pandelControl handler
	*/
	onPanelControlClick(e) {
		e.preventDefault();
		var target = e.currentTarget;
		var selectedPanel = this.getHashFromEl(target);
		if(!selectedPanel || !this.html.querySelectorAll('#' + selectedPanel).length) {
			console.warn('No panel found with the #id: ', selectedPanel);
			return;
		}
		_each(this.html.querySelectorAll('.panel'), (el)=>{
			el.classList.toggle('is-active', el.id === selectedPanel);
		});
		_each(this.html.querySelectorAll(defaults.html.panelController), (el)=>{
			var selectedButton = this.getHashFromEl(el);
			el.classList.toggle('is-active', selectedButton === selectedPanel);
		});
		Events.setState(ev.panelChange, ['activePanel'], selectedPanel);
	}

	/**
	*  getHashFromEl
	*/
	getHashFromEl(el) {
		var result = el.hash && el.tagName.toLowerCase() === 'a' ? el.hash : el.getAttribute('data-href');
		return result.replace('#', '');
	}

	/**
	*  onGeoLocationClick
	*/
	onGeoLocationClick(e) {
		e.preventDefault();
		this.Progress.start({message: defaults.labels.getGeoLocation});
		if(this.geoLocationMarker) {
			this.geoLocationMarker.setMap(null);
			this.geoLocationMarker = null;
		}

		this.getUserCords().then(
			this.handleGeoLocation.bind(this),
			(error)=>{
				Events.setState(ev.setLocation, ['geoLocation'], false);
				this.Progress.stop();
				console.warn('GeoLocation: ', error);
			}
		);
	}

	/**
	*  getUserCords
	*  @private
	*/
	getUserCords() {
	    return new Promise((resolve, reject) => {
	        if(navigator.geolocation) {
    			navigator.geolocation.getCurrentPosition(pos => resolve(pos), reject, { enableHighAccuracy: true });
	        } else {
	        	reject(new Error('Geolocation API is not supported'));
	        }
	    });
	}

	/**
	*  onNoStoresInBound TODO: handle this situation
	*/
	noStoresInBound() {
		if(this.note) this.note.destroy();
		var data = this.getData();
		var store = this.setDistanceToCenter(data)[0];
		this.note = new Note({
				title: defaults.labels.noStoresFound.title,
				subTitle: defaults.labels.noStoresFound.subTitle,
				button: {
					text: defaults.labels.noStoresFound.button
				},
				onClick: (e)=>{
					e.preventDefault();
					this.expandBound(store);
					this.note.destroy();
				}
			});

		this.list.appendChild(this.note.html());
	}

	/**
	*  handleGeoLocation
	*/
	handleGeoLocation(position) {
		this.Progress.stop();
		var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		Events.setState(ev.setLocation, ['geoLocation'], latlng);
		var icon = new google.maps.MarkerImage(defaults.icons.geolocation.url, null, null, null, new google.maps.Size(defaults.icons.geolocation.w, defaults.icons.geolocation.h));
		this.geoLocationMarker = new google.maps.Marker({
		    position: latlng,
		    map: this.map,
		    title: defaults.labels.youAreHere,
		    icon: icon,
		    draggable: true,
		    client_id: 'you' // jshint ignore:line
 		});
		this.map.setCenter(latlng);
		this.map.setZoom(defaults.onGetGeolocationZoom);
		this.getStoresInBound();
	}

	/**
	*  expandBound - takes a storeModel and expands the bound to contain the provided store
	*/
	expandBound(storeModel) {
		var offset = 0.02;
		var bounds = this.map.getBounds();
		var pos = new google.maps.LatLng(storeModel.latitude - offset, storeModel.longitude - offset);
		bounds.extend(pos);
		this.map.setCenter(bounds.getCenter());
		this.map.fitBounds(bounds);
		this.getStoresInBound();
	}
	/**
	* onClick - global accessibility helper
	*
	*/
	onClickGlobal() {
		//remove accessible helper
		document.body.removeAttribute('data-focus-source');
	}

	/**
	* on change handler to the filter functionality
	*
	*/
	filterChange() {
		this.getStoresInBound();
	}

	/**
	* keyDownGlobal
	*
	*/
	onKeyDownGlobal(e) {
		//accessibility focus helper
		if(e.keyCode === ev.keyCode.tab ) {
			document.body.setAttribute('data-focus-source', 'keyboard');
		}

	}
	/**
	*  onSearchClick
	*/
	onSearchClick(e) {
		e.preventDefault();
		var service = new google.maps.places.AutocompleteService();
		service.getPlacePredictions({input: this.input.value}, (result)=>{
			this.getLocationFromAddress(result[0].description).then((place)=>{
				this.onPlaceChange(place[0]);
				this.input.value = place[0].formatted_address; // jshint ignore:line
			});
		});
	}
	static get events() {
		return Events;
	}
}
export { StoreLocator, defaults, ev};
