let defaults = {
	"uri": "//maps.googleapis.com/maps/api/js",
	"libraries": ['places', 'geometry'],
	"callbackFn": "initClientMap",
	"useScriptInject": true,
	"zoomLevelOnListClick": 17,
	"countryFilterOnInitialize": null,
	"countryFilterOnInitializeKey": null,
	"markerClusterer": true,
	"markerClustererStart": 6,
	"maxZoomLevelClusterer": 6,
	"zoomLevelFilterStoresInBound": 6,
	"askForGeoLocation": null,
	"fakeLoadingDelay": 0,
	"bounceTimeout": 2000,
	"animationDuration": 200,
	"transitionIn": "on",
	"transitionOut": "off",
	"spinnerUrl": "/static/puff.svg",
	"apiDataKey": null,
	"countryCode": "DK",
	"languageCode": "da",
	"onSetSelectionZoom": 13,
	"onGetGeolocationZoom": 13,
	"html": {
		"container": "#js-storelocator",
		"map": "[data-sl-map]",
		"sidebar": "[data-sl-sidebar]",
		"mapMobile": "[data-sl-mapmobile]",
		"list": "[data-sl-list]",
		"listItem": "[data-sl-listitem]",
		"listSize": "[data-sl-listsize]",
		"infoItem": "[data-sl-info]",
		"input": "[data-sl-input]",
		"searchButton": "[data-sl-placesearch]",
		"clearButton": "[data-sl-placeclear]",
		"filter": "[data-sl-filter]",
		"filterButton": "[data-sl-filterbutton]",
		"mainPanel": "[data-sl-mainpanel]",
		"geoLocation": "[data-sl-geolocation]",
		"overlayClose": "[data-sl-overlayclose]",
		"infoBox": "[data-el-infobox]",
		"storeAttribute": "[data-sl-storeattribute]",
		"panelController": "[data-sl-panelcontroller]",
		"storeMoreInfo": "[data-sl-storemoreinfo]"
		//"loaderElement": ".storelocator__loader"
	},
	"cssClass": {
		"isActive": "is-active",
		"isHover": "is-hover",
		"isHidden": "is-hidden",
		"isSelected": "is-selected",
		"visibilityHidden": 'visibility-hidden',
		"ready": "storelocator--ready",
		"storeItem": "storelocator__list-item",
		"storeItemInner": "storelocator__list-item-inner",
		"infoOverlay": "storelocator__info-overlay",
		"infoOverlayClose": "storelocator__info-overlay-close",
		"infoOverlayContent": "storelocator__info-overlay-content",
		"infoAddrHeader": "info-address__header",
		"infoSection": "info-section",
		"infoLinklist": "info-section__linklist",
		"infoAddress": "info-section__address",
		"infoHours": "info-section__hours",
		"progress": "storelocator-progress",
		"progressMsg": "storelocator-progress__message",
		"note": "storelocator-note",
		"noteSubTitle": "storelocator-note__subtitle"

	},
	"storeAttributes": [
		{
			"sid": "S1",
			"name": "Bang & Olufsen Stores",
			"markerIcon": {
				"url": "/static/beo-store-round.svg",
				"w": 40,
				"h": 40
			},
			"markerIconSelected": {
				"url": "/static/authorised-retailers-round.svg",
				"w": 40,
				"h": 40
			},
			"filterIcon": "/static/beo-store.svg",
			"weight": 2
		},
		{
			"sid": "S2",
			"name": "Authorised Retailers",
			"markerIcon": {
				"url": "/static/authorised-retailers-round.svg",
				"w": 40,
				"h": 40
			},
			"markerIconSelected": {
				"url": "/static/beo-store-round.svg",
				"w": 40,
				"h": 40
			},
			"filterIcon": "/static/authorised-retailers.svg",
			"weight": 2
		},
		{
			"sid": "S3",
			"name": "Other Dealers",
			"markerIcon": {
				"url": "/static/other-dealers-round.svg",
				"w": 40,
				"h": 40
			},
			"markerIconSelected": {
				"url": "/static/beo-store-round.svg",
				"w": 40,
				"h": 40
			},
			"filterIcon": "/static/other-dealers.svg",
			"weight": 3
		}
	],
	"icons": {
		"selected": { "url": "/static/marker-selected.svg", "w": 31, "h": 43 },
		"hover": { "url": "/static/marker-hover.svg", "w": 31, "h": 43 },
		"default": { "url": "/static/marker-default.svg", "w": 20, "h": 20 },
		"geolocation": { "url": "/static/geolocation.svg", "w": 24, "h": 39 },
		"places": { "url": "/static/marker-places.svg", "w": 20, "h": 20 },
		"cluster": { "url": "//developers.google.com/maps/documentation/javascript/examples/markerclusterer/m" },
		"info": { "url": "/static/info.svg" }

	},
	"mapOptions": {
		"mapTypeId": "client_style",
		"zoom": 7,
		"center": {
			"lat": 55.88,
			"lng": 9.72
		},
		"mapTypeControl": false
	},
	"services": {
		"stores": null,
		"details": null
	},
	"templates": {

	},

	"mapData": {
		"storeModel": (item)=>{
	    	return {
	    		address1: item.Address1,
	    		address2: item.Address3,
	    		zipCode: item.ZipCode,
	    		city: item.City,
	    		latitude: item.Latitude,
	    		longitude: item.Longitude,
	    		id: item.ID,
	    		sid: item.RetailerTypeID,
	    		countryCode: item.CountryISO,
	    		openingHours: Object.keys(item.OpeningHours).map((key)=>{ return { label: key, hours: item.OpeningHours[key] }; }),
	    		links: [
					{
						label: "",
						text: "Website",
						href: item.HomepageUrl
					},
					{
						label: "Mail: ",
						text: item.Email,
						href: 'mailto:' + item.Email
					},
					{
						label: "Phone: ",
						text: item.Phone,
						href: 'tel:' + item.Phone
					},
				]
	    	}
		},
		"detailModel": (item)=>{
			return {
				address1: item.Address1,
	    		address2: item.Address3,
	    		zipCode: item.ZipCode,
	    		city: item.City,
				distance: null,
				openingHours: Object.keys(item.OpeningHours).map((key)=>{ return { label: key, hours: item.OpeningHours[key] }; }),
				links: [
					{
						label: "Website",
						title: "website",
						href: item.HomepageUrl
					},
					{
						label: "Mail",
						title: "mail",
						href: item.Email
					},
					{
						label: "phone",
						title: "phone",
						href: 'tel:' + item.Phone
					},
				]
			}
		}
	},
	"labels": {
		"introText": "Map is being build - please wait...",
		"getGeoLocation": "We are trying to find you'",
		"noStoresInBound": "We couldn't find a store in this area - found the nearest for you",
		"distance": "away from you",
		"distanceUnit": "km.",
		"overlayClose": 'Close',
		"moreInfo": 'More info',
		"youAreHere": 'Your location',
		"yourSearchMarkerLabel": 'Your search',
		"noStoresFound": {
			"title": 'No stores found',
			"subtitle": 'Vi fandt desværre ingen butikker, du kan udvide søgningen eller finde en butik nær dig.',
			"button": 'Udvid søgning'
		}

	}
}

//Internal event strings
const ev = {
	stateChanged: 'state:changed',
	mapReady: 'map:ready',
	markerClick: 'marker:click',
	listItemClick: 'store:click',
	setSelected: 'store:setselected',
	selectedStore: 'store:selectedstore',
	clearList: 'map:clearlist',
	noStoresInBound: 'map:nostoresinbound',
	showInfo: 'store:showinfo',
	hideInfo: 'inforoverlay:close',
	setFilter: 'storeattribute:select',
	setStoreData: 'app:setstoredata',
	setLocation: 'places:location',
	panelChange: 'panels:change',
	drag: 'map:drag',
	keyCode:{
		tab: 9
	},
}

export { ev, defaults }
