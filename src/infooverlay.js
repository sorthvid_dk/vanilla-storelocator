import { defaults } from './index';
import { ev } from './settings';
import MicroTemplate from './microtemplate';
import Events from './events';
import { formatSelector as _css} from './utils';

class InfoOverlay extends MicroTemplate {
	constructor(data){
		super(data);
		var events = {};
		events['click ' + defaults.html.overlayClose] = 'onClose';
		this.delegateEvents(events);

		Events.on(ev.mapReady, ()=>{
			this.el.classList.toggle('is-fixed', Events.state.isSmallScreen);
		});
		this.isActive = false;
	}

	onClose(e) {
		e.preventDefault();
		this.isActive = false;
		Events.emit(ev.hideInfo);
		this.el.classList.add(defaults.transitionOut);
	}

	onTransitionEnd(e) {
		if(!this.isActive && e.propertyName === 'opacity') {
			this.destroy();
		}
	}

	/**
	*  setActive - set components state to active
	*/
	setActive() {
		this.isActive = true;
	}

	/**
	*  getGoogleLink
	*  @private
	*  @param data {Object} - detailModel
	*/
	getGoogleLink(data) {
		var base = '//maps.google.com/?';
		var daddr = 'daddr=' + '+' + data.address2.replace(/ /g, '+') + '+' + data.city.replace(/ /g, '+') + '+' + data.zipCode;
		var saddr = Events.state.geoLocation ? '&saddr=Current Location' : '';
		return base + daddr + saddr;
	}
	/*jshint quotmark: double */
	render(data) {
		if(Object.keys(data).length === 0 && data.constructor === Object) return null;
		var geoDistance = Events.state.geoLocation ? `<span>${data.distanceToGeoLocation + ' ' + defaults.labels.distanceUnit + ' ' + defaults.labels.distance} </span><br />` : '';
		var css = Events.state.isSmallScreen ? ' is-fixed' : '';
		var links = data.links.map(link=>{
			return `<li>${link.label}<a href="${link.href}">${link.text}</a></li>`;
		});
		var openingHours = data.openingHours.map(link=>{
			return `<dt>${link.label}</dt>
			<dd>${link.hours}</dd>`;
		});
		return `<div class="${_css(defaults.cssClass.infoOverlay) + css}">
			<button class="${_css(defaults.cssClass.infoOverlayClose)}" ${_css(defaults.html.overlayClose)}>
				<svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    			<g id="Page-1" fill-rule="evenodd">
    			<polygon id="close" fill="#111111" points="15 14.2848003 14.2924528 14.9989898 7.5 8.21368442 0.70754717 15 0 14.2858105 6.78234501 7.49949492 0 0.714189508 0.70754717 7.1776827e-15 7.5 6.78631558 14.2924528 0 15 0.714189508 8.21765499 7.49949492"></polygon>
    			</g>
    			</svg>
    			<span aria-hidden="true" class="${_css(defaults.cssClass.visibilityHidden)}">${defaults.labels.overlayClose}</span>
			</button>
			<div class="${_css(defaults.cssClass.infoOverlayContent)}">
				<address class="${_css(defaults.cssClass.infoSection) + ' ' + _css(defaults.cssClass.infoAddress)}">
					<span class="${_css(defaults.cssClass.infoAddrHeader)}">${data.address1}</span>
					${data.address2}<br />
					${data.zipCode} ${data.city}<br />
					${geoDistance}

				</address>
				<div class="${_css(defaults.cssClass.infoSection)}">
					<ul class="${_css(defaults.cssClass.infoLinklist)}">
						<li> <a href="${this.getGoogleLink(data)}" target="_blank">Get Direction</a></li>
						${links.join('')}

					</ul>
				</div>
				<div class="${_css(defaults.cssClass.infoSection)}">
					<dl class="${_css(defaults.cssClass.infoHours)}">
						${openingHours.join('')}
					</dl>
				</div>
			</div>
		</div>`;
	}

}

export default InfoOverlay;
