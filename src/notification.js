import MicroTemplate from './microtemplate';
import { formatSelector as _cssSelector} from './utils';
class Notification extends MicroTemplate {
	constructor(data, host){
		super(data);
		this.host = host || {};
		this.delegateEvents({
			'transitionend': 'onTransitionEndRemove',
			'webkitTransitionEnd': 'onTransitionEndRemove',
			'button click': 'onTransitionEndRemove'
		});
	}

	onTransitionEndRemove() {
		this.el.classList.remove('fade-out');
		super.destroy();
	}

	show(data) {
		if(data) {
			this.reRender(data);
		}
		this.host.append(this.el);
	}

	remove() {
		this.el.classList.add('fade-out');
	}
	/*jshint quotmark: double */
	render(data) {
		var message = data.message || '';
		return `<div class="storelocator-notification ${_cssSelector('storelocator-notification--' + data.type)}">
				<button>
					<svg class="svg-icon" viewBox="0 0 15 15" xmlns="http://www.w3.org/2000/svg"><path fill="#111" d="M15 14.285l-.708.714L7.5 8.213.708 15 0 14.286 6.782 7.5 0 .713.708 0 7.5 6.786 14.292 0 15 .714 8.218 7.5" fill-rule="evenodd"/></svg>
				</button>
				<p mv-message>${message}</p>
		</div>`;
	}
}

export default Notification;
