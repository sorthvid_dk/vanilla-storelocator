import { defaults } from './index';
import { ev } from './settings';
import MicroTemplate from './microtemplate';
import Events from './events';

class StoreAttribute extends MicroTemplate { //storeAttributeModel
	constructor(data) {
		super(data);
		var _listners = {
			click: 'onClick'
		};
		this.delegateEvents(_listners);
		this.isSelected = false;
	}
	onClick(e){
		e.preventDefault();
		this.isSelected = !this.isSelected;
		var activeFilters = [].concat(Events.state.activeFilters);

		if(this.isSelected){
			activeFilters.push(this.data.sid);
		} else {
			//activeFilters = Immutable.without(activeFilters, this.data.id);
			activeFilters = activeFilters.filter(item=>{
				return item != this.data.sid;
			});
		}
		Events.setState(ev.setFilter, ['activeFilters'], activeFilters, true);
		this.el.classList.toggle(defaults.cssClass.isSelected, this.isSelected);
	}
	render(data){
		return `<a href="#" role="button">
			<img src="${data.filterIcon}" /><span>${data.name}</span>
		</a>`;
	}
}

export default StoreAttribute;
